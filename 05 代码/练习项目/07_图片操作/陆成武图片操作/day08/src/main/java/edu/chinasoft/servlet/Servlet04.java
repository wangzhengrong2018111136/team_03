package edu.chinasoft.servlet;

import edu.chinasoft.util.VCodeGenerator;

import javax.imageio.ImageIO;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

@WebServlet(name = "servlet04", value = "/servlet/servlet04")
public class Servlet04 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 生成一张图片
        VCodeGenerator vCodeGenerator = new VCodeGenerator();
        String vcode = vCodeGenerator.generatorVCode();
        //  生成一张  .gif的图片
        BufferedImage vImg = vCodeGenerator.generatorRotateVCodeImage(vcode, true);

        // 通过响应对象把图片返回给浏览器
        response.setHeader("content-type","image/gif");
        ImageIO.write(vImg, "gif", response.getOutputStream());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
