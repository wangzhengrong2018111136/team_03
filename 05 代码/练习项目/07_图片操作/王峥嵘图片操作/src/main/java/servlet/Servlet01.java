package servlet;
import org.apache.log4j.Logger;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;


@WebServlet(name = "servlet01", value = "/servlet/servlet01")
public class Servlet01 extends HttpServlet {
    private Logger logger = Logger.getLogger(Servlet01.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("Servlet01>>>doGet start ……");
        // 获取get请求里的数据
        String result01 = request.getParameter("key01");
        String result02 = request.getParameter("key02");

        logger.debug("key01:"+result01);
        logger.debug("key02:"+result02);

        logger.debug("Servlet01>>>doGet end ……");


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("Servlet01>>>doPost start ……");
        //  解决post请求数据乱码问题
        request.setCharacterEncoding("utf-8");



        String result01 = request.getParameter("user_name");
        String result02 = request.getParameter("user_password");
        String result03 = request.getParameter("user_sex");


        String[] user_inters = request.getParameterValues("user_inter");


        String csd = request.getParameter("csd");


        logger.debug("user_name:"+result01);
        logger.debug("user_password:"+result02);
        logger.debug("user_sex:"+result03);


        logger.debug("所有的兴趣如下：");
        for(String element:user_inters){
            logger.debug(element);
        }

        logger.debug("出生地："+csd);


        logger.debug("Servlet01>>>doPost end ……");
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jump.html");
        requestDispatcher.forward(request,response);
    }
}
