package servlet;

import util.VCodeGenerator;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;

@WebServlet(name = "servlet03", value = "/servlet/servlet03")
public class Servlet03 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 生成一张图片
        VCodeGenerator vCodeGenerator = new VCodeGenerator();
        String vcode = vCodeGenerator.generatorVCode();
        //  生成一张  .gif的图片
        BufferedImage vImg = vCodeGenerator.generatorRotateVCodeImage(vcode, true);

        // 通过响应对象把图片返回给浏览器
        response.setHeader("content-type","image/gif");
        ImageIO.write(vImg, "gif", response.getOutputStream());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
