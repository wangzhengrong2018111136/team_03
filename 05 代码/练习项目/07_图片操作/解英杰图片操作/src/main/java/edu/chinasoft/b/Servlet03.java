package edu.chinasoft.b;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@WebServlet(name = "servlet03", value = "/servlet/servlet03")
public class Servlet03 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // 通过响应头  告诉浏览器返回的是一张图片
        response.setHeader("content-type","image/jpg");


        // 获取关于图片的内容    输入流
        ServletContext servletContext = this.getServletContext();
        String imagePath = servletContext.getRealPath("/WEB-INF/classes/upload/image01.jpg");

        InputStream fin = new FileInputStream(imagePath);
        int lenght = -1;
        byte[] buff = new byte[1024];


        // 把图片的内容通过浏览器输出流输出给浏览器   字节流
        OutputStream fout = response.getOutputStream();

        while ((lenght = fin.read(buff)) != -1) {

            fout.write(buff, 0, lenght);

        }


        fin.close();
        fout.close();


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
