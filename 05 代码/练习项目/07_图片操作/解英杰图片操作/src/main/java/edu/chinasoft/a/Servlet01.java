package edu.chinasoft.a;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.OutputStream;

@WebServlet(name = "servlet01", value = "/sevlet/servlet01")
public class Servlet01 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //  以字节为单位   把数据  “response content”
        String  content="我是1名 student!";
        //  获取关于浏览器的输出流
        OutputStream outputStream = response.getOutputStream();

        // 通过输出流把内容输出给浏览器
        outputStream.write(content.getBytes());

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
