package edu.chinasoft.a;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "servlet02", value = "/servlet/servlet02")
public class Servlet02 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String conent="我是一名西北大学的学生！";

        // 设置服务器端编码格式为
        response.setCharacterEncoding("utf-8");
        // 设置浏览器端的编码格式
        response.setContentType("text/html;charset=utf-8");


        
        // 获取关于浏览器的字符输出流
        PrintWriter out = response.getWriter();

        // 通过字符串输出流把内容输出浏览器
        out.write(conent);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
