package edu.chinasoft.pojo;

public class Student {
    private int id;
    private String studentName;
    private String studentSex;


    public Student() {
    }

    public Student(int id, String studentName, String studentSex) {
        this.id = id;
        this.studentName = studentName;
        this.studentSex = studentSex;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentSex() {
        return studentSex;
    }

    public void setStudentSex(String studentSex) {
        this.studentSex = studentSex;
    }
}
