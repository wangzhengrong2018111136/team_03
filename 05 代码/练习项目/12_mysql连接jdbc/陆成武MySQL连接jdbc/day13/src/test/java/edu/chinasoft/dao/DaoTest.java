package edu.chinasoft.dao;

import edu.chinasoft.pojo.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationcontext.xml")
public class DaoTest {

    @Autowired
    @Qualifier(value="dataSource")
    private DataSource dataSource;

    @Autowired
    @Qualifier(value="studentDao")
    private StudentDao studentDao;

    @Test
    public void testMethod01(){
        System.out.println("==============");
        System.out.println(dataSource);
    }

    @Test
    public void testMethod02(){
        Student student = new Student();
        student.setStudentName("cjgong04");
        student.setStudentSex("m");
        studentDao.insertTable(student);
    }

}