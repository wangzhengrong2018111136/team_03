package edu.chinasoft.dao;

import edu.chinasoft.pojo.Student;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import javax.sql.DataSource;

@Repository(value="studentDao")
public class StudentDaoImp implements StudentDao {
    /*
       jsp+servlet+javabean:  数据库访问
             步骤1：自定义了一个jdbc工具类
             步骤2：在每个方法里通过jdbc工具类去操作表
       spring框架：   数据库访问
              提供了关于jdbc的工具类（功能强大，性能比较高）    JdbcTemplate
              使用到了数据库连接池
       步骤1：添加相应的jar
            mysql驱动
            c3p0
       步骤2：配置数据源
       步骤3：在dao层代码初始化工具类对象jdbcTemplate
       步骤4：在dao层的各个功能方法可以通过jdbcTemplate工具类实现crud功能

     */

    // jdbc 工具类
    private JdbcTemplate jdbcTemplate;

    @Resource(name="dataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public  void   insertTable(Student student){
        String sql="INSERT  INTO  `t_student`(t_student.`student_name`,t_student.`student_sex`) VALUES(?,?);";
        this.jdbcTemplate.update(sql,student.getStudentName(),student.getStudentSex());
    }


}
