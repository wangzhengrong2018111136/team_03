package edu.chinasofe.d;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "servlet10", value = "/servlet/servlet10")
public class Servlet10 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //  获取Session对象

        HttpSession session = request.getSession(false);

        String result=null;
        if(session!=null){
            Object shop = session.getAttribute("shop");
            if(shop!=null){
                session.setAttribute("shop","dsj");
                result="商品修改成功！";
            }

        }
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");

        PrintWriter out = response.getWriter();

        if(result!=null){
            out.write(result);
        }else{
            out.write("不好意思，您没有购买任何商品！");
        }




    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
