package edu.chinasofe.c;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "servlet07", value = "/servlet/servlet07")
public class Servlet07 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        HttpSession session01 = request.getSession();
//        HttpSession session02 = request.getSession();
//
//        if(session01==session02){
//            System.out.println("同一个对象");
//        }else{
//            System.out.println("不是同一个对象");
//        }
        HttpSession session02 = request.getSession();
        HttpSession session01 = request.getSession(false);

        System.out.println(session01);
        System.out.println(session02);

        // 删除session对象
        session02.invalidate();
        //session01.invalidate();
        HttpSession session03 = request.getSession();
        System.out.println(session03);




    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
