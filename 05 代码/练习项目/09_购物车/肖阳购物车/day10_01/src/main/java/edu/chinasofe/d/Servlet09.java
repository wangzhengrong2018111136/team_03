package edu.chinasofe.d;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "servlet09", value = "/servlet/servlet09")
public class Servlet09 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");

        PrintWriter out = response.getWriter();
        // 获取Session里的数据
        HttpSession session = request.getSession(false);
        String result=null;

        if(session!=null){
            Object shop = session.getAttribute("shop");
            if(shop!=null){
                result=(String)shop;
            }
        }

        if(result==null){
            out.write("不好意思，您没有购买任何商品！");

        }else{
            out.write("您购买的商品为：<br>");
            out.write(result);
        }




    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
