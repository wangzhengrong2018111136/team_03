package edu.chinasofe.b;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "servlet05", value = "/servlet/servlet05")
public class Servlet05 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 删除cookie里的数据
        // 步骤1：先要获取所要删除的cookie
        Cookie[] cookieArray = request.getCookies();

        String result = null;
        if (cookieArray != null) {
            for (Cookie cookie : cookieArray) {
                if (cookie.getName().equals("shop")) {
                    // 步骤2：把cookie的保存时间修改成0
                    cookie.setMaxAge(0);
                    cookie.setPath(request.getContextPath());
                    response.addCookie(cookie);
                    result = "商品删除成功！";
                }
            }

        }
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");

        PrintWriter out = response.getWriter();
        if (result == null) {
            out.write("不好意思，您没有购买任何商品！");
        } else {
            out.write(result);
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
