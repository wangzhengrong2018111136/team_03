package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "servlet04", value = "/servlet/servlet04")
public class Servlet04 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //  获取Session对象

        HttpSession session = request.getSession(false);

        String result=null;
        if(session!=null){
            Object shop = session.getAttribute("shop");
            if(shop!=null){
               // session.invalidate();    //  删除整个Session对象
                session.removeAttribute("shop");  // 删除session容器里的shop元素
                result="商品删除成功！";
            }

        }
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");

        PrintWriter out = response.getWriter();

        if(result!=null){
            out.write(result);
        }else{
            out.write("不好意思，您没有购买任何商品！");
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
