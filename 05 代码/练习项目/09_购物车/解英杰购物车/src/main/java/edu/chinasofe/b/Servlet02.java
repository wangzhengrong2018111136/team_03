package edu.chinasofe.b;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "servlet02", value = "/servlet/servlet02")
public class Servlet02 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 把商品添加到cookie
        Cookie cookie = new Cookie("shop","dsj");
        // 设置cookie在浏览器端的保存时间
        cookie.setMaxAge(1*60*60*24);
        cookie.setPath(request.getContextPath());
        // 把cookie发送给浏览器
        response.addCookie(cookie);

        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");

        PrintWriter out = response.getWriter();
        out.write("购买商品成功！");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
