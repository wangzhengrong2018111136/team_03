package edu.chinasofe.b;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "servlet04", value = "/servlet/servlet04")
public class Servlet04 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //步骤1：先查找到所要修改的cookie
        Cookie[] cookieArrary = request.getCookies();
        String result=null;
        if(cookieArrary!=null){
            for(Cookie cookie:cookieArrary){
                if(cookie.getName().equals("shop")){
                    //步骤2：修改cookie里的value值[修改cookie里的value值]
                    cookie.setValue("xyj");
                    cookie.setMaxAge(1*60*60*24);
                    cookie.setPath(request.getContextPath());
                    response.addCookie(cookie);
                    result="商品修改成功！";

                }
            }
        }
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");

        PrintWriter out = response.getWriter();
        if(result==null){
            out.write("不好意思，您没有购买商品！");
        }else{
            out.write(result);
        }




    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
