package edu.chinasofe.b;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "servlet03", value = "/servlet/servlet03")
public class Servlet03 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 获取cookie里数据
        // 获取所有的cookie对象
        Cookie[] cookieArray = request.getCookies();

        String result=null;

        if(cookieArray!=null){
            for(Cookie cookie:cookieArray){
                // 获取cookie里的key值
                if(cookie.getName().equals("shop")){
                    result= cookie.getValue();      // 获取cookie里的value值
                    break;       // 退出循环
                }

            }

        }
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");

        PrintWriter out = response.getWriter();
        if(result==null){
            out.write("不好意思，您没有购买任何商品！");
        }else{
            out.write("您购买的商品如下：<br>");
            out.write(result);
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
