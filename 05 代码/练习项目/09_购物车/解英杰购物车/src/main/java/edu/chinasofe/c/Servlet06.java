package edu.chinasofe.c;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "servlet06", value = "/servlet/servlet06")
public class Servlet06 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 创建Session对象
        /*
          创建Session对象
          步骤1：检查有没有关于当前会话的session，
          步骤2：
               如果有，则获取。
               如果没有，则创建一个新Session对象


         */
        HttpSession session01 = request.getSession();
        HttpSession session02 = request.getSession(true);
        /*
          获取Session对象
          步骤1：检查有没有关于当前会话的session，
          步骤2：
               如果有，则获取。
               如果没有，则会返回一个null。


         */
        HttpSession session03 = request.getSession(false);


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
