<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/5/21
  Time: 15:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page pageEncoding="UTF-8" language="java" isErrorPage="true" %>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<%--可以获取其他8个对象--%>
<%
%>

<%--
pageContext 域容器工具类

Servlet   sevletcontext   request   cookie   session
jsp       appliation    session    request   page

--%>

<%
    // 向域page里添加数据
    pageContext.setAttribute("key01","value01");
    // 指定保存容器的范围   保存到Session里
    pageContext.setAttribute("key01","value01",PageContext.SESSION_SCOPE);

    // 从page域里取出key01对应的值
    pageContext.getAttribute("key01");
    // 从session域里取出key01对应的值
    pageContext.getAttribute("key01",PageContext.SESSION_SCOPE);

    /*
      从域里把数据取出来
      page    request     session    application    null





     */
    pageContext.findAttribute("key01");

%>





</body>
</html>
