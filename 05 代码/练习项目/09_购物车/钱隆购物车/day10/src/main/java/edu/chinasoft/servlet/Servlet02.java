package edu.chinasoft.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "servlet02", value = "/servlet/servlet02")
public class Servlet02 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");

        PrintWriter out = response.getWriter();
        // 获取Session里的数据
        HttpSession session = request.getSession(false);
        String result=null;

        if(session!=null){
            Object shop = session.getAttribute("shop");
            if(shop!=null){
                result=(String)shop;
            }
        }

        if(result==null){
            out.write("不好意思，您没有购买任何商品！");

        }else{
            out.write("您购买的商品为：<br>");
            out.write(result);
        }




    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
