package user.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;


import org.apache.log4j.Logger;
import user.pojo.User;
import user.service.UserService;
import user.service.UserServiceImp;

@WebServlet(name = "servletforuserupdte", value = "/servlet/servletforuserupdte")
public class ServletForUserUpdte extends HttpServlet {
    private UserService userService=new UserServiceImp();
    private  Logger logger = Logger.getLogger(ServletForUserUpdte.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.debug("ServletForUserUpdte>>>doPost start ……");
        //获取页面中需要修改信息
        String idString = request.getParameter("user_id");
        String userName = request.getParameter("user_name");
        String userSex = request.getParameter("user_sex");


        User user = new User();
        user.setUserId(Integer.parseInt(idString));
        user.setUserName(userName);
        user.setUserSex(userSex);


        //调用服务层方法实现用户信息修改
        userService.modifyUser(user);




        //跳转到显示页面

        logger.debug("ServletForUserUpdte>>>doPost end ……");
        response.sendRedirect("/User_war/servlet/servletforuserselect");

    }
}
