package chinasoft.service;


import chinasoft.pojo.Student;

public interface StudentService {
    // 把学生信息写入到数据库
    void register(Student student);
}
