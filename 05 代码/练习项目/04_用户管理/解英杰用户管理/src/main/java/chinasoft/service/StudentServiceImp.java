package chinasoft.service;


import chinasoft.dao.StudentDao;
import chinasoft.dao.StudentDaoImp;
import chinasoft.pojo.Student;
import org.apache.log4j.Logger;


public class StudentServiceImp implements StudentService {
    private Logger logger = Logger.getLogger(StudentServiceImp.class);
    // 接口编程
    private StudentDao studentDao=new StudentDaoImp();

    // 把学生信息写入到数据库
    @Override
    public void register(Student student){
        logger.debug("StudentServiceImp>>>register start ……");
        studentDao.insert(student);
        logger.debug("StudentServiceImp>>>register end ……");
    }

}
