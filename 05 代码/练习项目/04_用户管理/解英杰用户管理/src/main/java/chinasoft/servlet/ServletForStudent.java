package chinasoft.servlet;

import chinasoft.pojo.Student;
import chinasoft.service.StudentService;
import chinasoft.service.StudentServiceImp;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "servletforstudent", value = "/servlet/servletforstudent")
public class ServletForStudent extends HttpServlet {
    private Logger logger = Logger.getLogger(ServletForStudent.class);
    private StudentService studentService=new StudentServiceImp();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForStudent>>>doPost start……");



        // 获取页面提交的数据
        String studentName = request.getParameter("student_name");
        String studentSex = request.getParameter("student_sex");
        logger.debug(studentName+":"+studentSex);
        // 封装到学生类
        Student student = new Student();
        student.setStudentName(studentName);
        student.setStudentSex(studentSex);
        // 调用service层的方法，把数据插入到数据库
        studentService.register(student);

        logger.debug("ServletForStudent>>>doPost end……");
        // 如果注册成功，跳转到注册成功页面
        response.sendRedirect("/register_success.jsp");
        

    }
}
