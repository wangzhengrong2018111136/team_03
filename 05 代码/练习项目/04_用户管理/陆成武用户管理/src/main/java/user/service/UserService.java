package user.service;
import user.pojo.User;

import java.util.List;

public interface UserService {
    void register(User user);

    void  deleteUserById(User user);
    List<User> findAllUser();
    User  findUserById(User user);
    void  modifyUser(User user);
}
