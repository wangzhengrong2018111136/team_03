package user.pojo;

public class User {
    private  int  user_Id;
    private  String user_name;
    private String user_sex;

    public User() {
    }
    public User(int user_Id,String user_name,String user_sex){
        this.user_Id = user_Id;
        this.user_name = user_name;
        this.user_sex = user_sex;
    }
    public int getUserId() {
        return user_Id;
    }

    public void setUserId(int id) {
        user_Id = id;
    }

    public String getUserName() {
        return user_name;
    }

    public void setUserName(String name) {
        user_sex = name;
    }

    public String getUserSex() {
        return user_sex;
    }

    public void setUserSex(String sex) {
        user_sex = sex;
    }
}
