package user.service;

import user.dao.UserDao;
import user.dao.UserDaoImp;
import org.apache.log4j.Logger;
import user.pojo.User;

import java.util.List;


public class UserServiceImp implements UserService {
    private Logger logger = Logger.getLogger(UserServiceImp.class);
    // 接口编程
    private UserDao userDao=new UserDaoImp();

    // 信息写入到数据库
    @Override
    public void register(User user){
        logger.debug("UserServiceImp>>>register start ……");
        userDao.insert(user);
        logger.debug("UserServiceImp>>>register end ……");
    }

    @Override
    public List<User> findAllUser(){
        logger.debug("UserServiceImp>>>findAllUser start ……");
        List<User> result = userDao.select();
        logger.debug("UserServiceImp>>>findAllUser end ……");
        return result;

    }

    @Override
    public void  deleteUserById(User user){
        logger.debug("UserServiceImp>>>deleteUserById start ……");
        userDao.delelte(user);
        logger.debug("UserServiceImp>>>deleteUserById end ……");

    }

    @Override
    public User  findUserById(User user){
        logger.debug("UserServiceImp>>>findUserById start ……");
        User result = userDao.selectUserById(user);
        logger.debug("UserServiceImp>>>findUserById end ……");
        return result;
    }

    @Override
    public void  modifyUser(User user){
        logger.debug("UserServiceImp>>>modifyUser start ……");
        userDao.update(user);
        logger.debug("UserServiceImp>>>modifyUser end ……");

    }

}



