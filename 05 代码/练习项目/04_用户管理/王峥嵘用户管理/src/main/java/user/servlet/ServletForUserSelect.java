package user.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import user.pojo.User;
import user.service.UserService;
import user.service.UserServiceImp;

@WebServlet(name = "servletforuserselect", value = "/servlet/servletforuserselect")
public class ServletForUserSelect extends HttpServlet {
    private UserService userService=new UserServiceImp();
    private  Logger logger = Logger.getLogger(ServletForUserSelect.class);


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForUserSelect>>>doGet  start……");
        //获取所有的用户信息
        List<User> userList =  userService.findAllUser();


        //把信息保存到session
        HttpSession session = request.getSession();
        session.setAttribute("alluser",userList);

        logger.debug("ServletForUserSelect>>>doGet  end……");
        //跳转到shower
        response.sendRedirect("/User_war/showuser.jsp");




    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

