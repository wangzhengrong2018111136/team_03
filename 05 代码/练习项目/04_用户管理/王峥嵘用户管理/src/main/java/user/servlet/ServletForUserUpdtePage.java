package user.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;


import org.apache.log4j.Logger;
import user.pojo.User;
import user.service.UserService;
import user.service.UserServiceImp;

@WebServlet(name = "servletforuserupdtepage", value = "/servlet/servletforuserupdtepage")
public class ServletForUserUpdtePage extends HttpServlet {
    private UserService userService=new UserServiceImp();
    private Logger logger = Logger.getLogger(ServletForUserUpdtePage.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForUserUpdtePage>>>doGet start ……");

        //获取所要更新的用户id
        String idString = request.getParameter("id");

        //调用服务层方法，获取所要修改的信息
        User user = new User();
        user.setUserId(Integer.parseInt(idString));
        User userResult = userService.findUserById(user);

        //把信息保存到容器
        HttpSession session = request.getSession();
        session.setAttribute("userupdate",userResult);


        //跳转到更新页面
        logger.debug("ServletForUserUpdtePage>>>doGet end ……");

        response.sendRedirect("/User_war/update.jsp");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

