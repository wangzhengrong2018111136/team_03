package user.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import org.apache.log4j.Logger;
import user.pojo.User;
import user.service.UserService;
import user.service.UserServiceImp;

@WebServlet(name = "servletforuseradd", value = "/servlet/servletforuseradd")
public class ServletForUserAdd extends HttpServlet {
    private Logger logger = Logger.getLogger(ServletForUserAdd.class);
    private UserService userService=new UserServiceImp();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForUser>>>doPost start……");



        // 获取页面提交的数据
        int userId = Integer.parseInt(request.getParameter("id"));
        String userName = request.getParameter("name");
        String userSex = request.getParameter("sex");
        logger.debug(userName+":"+userSex);

        User user = new User();
        user.setUserId(userId);
        user.setUserName(userName);
        user.setUserSex(userSex);
        // 调用service层的方法，把数据插入到数据库
        userService.register(user);

        logger.debug("ServletForUser>>>doPost end……");
        // 如果注册成功，跳转到注册成功页面
        response.sendRedirect("/User_war/register_success.jsp");


    }
}

