<%--
  Created by IntelliJ IDEA.
  User: 王峥嵘
  Date: 2021/5/16
  Time: 11:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java"  pageEncoding="utf-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>显示学生信息</title>
</head>
<body>

<h3 align="center">学生信息</h3>
<table align="center" width="60%" border="1px" cellpadding="0px" cellspacing="0px">
    <tr align="center">
        <td>学生id</td>
        <td>学生姓名</td>
        <td>学生的性别</td>
        <td>操作</td>
    </tr>

    <c:forEach items="${alluser}" var="userelement">
        <tr align="center">
            <td>${userelement.userId}</td>
            <td>${userelement.userName}</td>
            <td>${userelement.userSex=='m'?'男':'女'}</td>
            <td>
                <a href="/User_war/servlet/servletforuserdelete?id=${userelement.userId}" onclick="return method01()">删除</a>
                |
                <a href="/User_war/servlet/servletforuserupdtepage?id=${userelement.userId}">更新</a></td>
        </tr>
    </c:forEach>

</table>


<script type="text/javascript">
    function method01(){
        var result=window.confirm("确定删除吗？");
        return result;
    }




</script>



</body>
</html>

