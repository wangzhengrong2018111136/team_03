<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + "/newdir/";
    request.setAttribute("basePath", basePath);
%>
<html>
<head>
    <title>文件上传页面</title>
</head>
<body>
<h1>文件上传地址</h1>
<p>${basePath}</p>
<c:forEach var="filename" items="${pathNames}">
    <p><a href="${basePath}${filename}">${filename}</a></p>
    <p><img src="${basePath}${filename}" width="200px"></p>
</c:forEach>
</body>
</html>
