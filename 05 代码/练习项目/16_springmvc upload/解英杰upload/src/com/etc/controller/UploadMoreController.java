package com.etc.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Johnny
 * 多文件上传
 */
@Controller
public class UploadMoreController {

    Logger logger = Logger.getLogger(UploadController.class);

    @RequestMapping("/touploadplus2")
    public String touploadplus(){
        logger.info("进入多文件上传");
        return "admin/upload_plus_more2";
    }

    @RequestMapping("/uploadplus2")
    public String uploadplus(@RequestParam("file") CommonsMultipartFile[] files, Model model) {
        if (files != null) {
            //定义集合存放文件名称集合
            List<String> pathNames = new ArrayList<>();
            for (CommonsMultipartFile file : files) {
                try {
                    String origname = file.getOriginalFilename();
                    logger.info(file);
                    logger.info(origname);
                    //创建文件存放的路径
                    String path = "D:/newdir/" + origname;
                    //创建文件对象
                    File newfile = new File(path);
                    //将前端传入的文件，写入到指定路径下的文件里
                    file.transferTo(newfile);
                    //将文件路径放入集合当中
                    pathNames.add(origname);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            model.addAttribute("pathNames",pathNames);
        }
        return "success_more";
    }

}
