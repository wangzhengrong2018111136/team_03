package com.etc.controller;

import java.io.File;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * @author Johnny
 * @category 多文件上传
 */
@Controller
public class FileUploadMoreController {

	@RequestMapping("touploadplusmore")
	public String toUploadplus(){
		return "admin/upload_plus_more";
	}

	@RequestMapping("uploadplusmore")
	public String uploadplus(@RequestParam("file") CommonsMultipartFile[] files,HttpServletRequest request){
		if(files!=null){
			for (CommonsMultipartFile file : files) {
				if(file.getSize()!=0){
					File file2 = getFile(file, request);
					getFileUrlPath(file2, request);
					try {
						file.getFileItem().write(file2);//将上传的文件写入新建的文件中
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			return "success";
		}else{
			return "error";
		}
	}

	//创建文件的上传物理路径的目标文件
	private File getFile(CommonsMultipartFile file,HttpServletRequest request){
		String path = request.getServletContext().getRealPath("/upload/");//获取本地存储路径
		System.out.println(path);
		String fileName = file.getOriginalFilename();//得到文件名称
		String fileType = fileName.substring(fileName.lastIndexOf("."));
		System.out.println(fileType);
		File file2 = new File(path, new Date().getTime()+fileType);//新建一个文件
		return file2;
	}

	//得到目标文件的url访问地址
	private void getFileUrlPath(File file2,HttpServletRequest request){
		String path = request.getContextPath();//得到项目名称,然后加上springmvc-servlet.xml配置的path地址
		System.out.println(path);
		path += "/upload/"+ file2.getName();
		System.out.println(path);
		request.setAttribute("fileName", file2.getName());
		request.setAttribute("filePath", path);
	}
}
