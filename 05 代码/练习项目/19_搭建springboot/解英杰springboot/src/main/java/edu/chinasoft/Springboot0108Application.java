package edu.chinasoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot0108Application {

	public static void main(String[] args) {
		SpringApplication.run(Springboot0108Application.class, args);
	}

}
