package edu.chinasoft.service;

import edu.chinasoft.dao.ImageDao;
import edu.chinasoft.dao.ImageDaoImp;
import edu.chinasoft.pojo.Image;
import org.apache.log4j.Logger;

import java.util.List;

public class ImageServiceImp implements ImageService {
    private Logger logger = Logger.getLogger(ImageServiceImp.class);
    private ImageDao imageDao=new ImageDaoImp();

    @Override
    public  void   uploadFile(Image image){
        logger.debug("ImageServiceImp>>>uploadFile start ……");
        imageDao.insertTable(image);
        logger.debug("ImageServiceImp>>>uploadFile end ……");
    }
    @Override
    public List<Image> findAllImage(){
        logger.debug("ImageServiceImp>>>findAllImage start ……");
        List<Image> result = imageDao.selectAll();
        logger.debug("ImageServiceImp>>>findAllImage start ……");
        return result;
    }


}
