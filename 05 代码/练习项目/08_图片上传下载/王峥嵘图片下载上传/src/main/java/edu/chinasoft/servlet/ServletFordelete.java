package edu.chinasoft.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import org.apache.log4j.Logger;
import edu.chinasoft.pojo.Image;
import edu.chinasoft.service.ImageService;
import edu.chinasoft.service.ImageServiceImp;


@WebServlet(name = "servletforimagedelete", value = "/servlet/servletforimagedelete")
public class ServletFordelete extends HttpServlet {
    private Logger logger = Logger.getLogger(ServletFordelete.class);
    private ImageService imageService=new ImageServiceImp();


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForImageDelete>>>doGet  start ……");
        // 获取删除的id
        String idString = request.getParameter("id");


        // 调用服务层方法删除
        Image image = new Image();
        image.setImageId(Integer.parseInt(idString.trim()));
        imageService.delelte(image);

        logger.debug("ServletForImageDelete>>>doGet  end ……");


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

