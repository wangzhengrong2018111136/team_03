package edu.chinasoft.servlet;

import edu.chinasoft.pojo.Image;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

import edu.chinasoft.service.ImageService;
import edu.chinasoft.service.ImageServiceImp;

@WebServlet(name = "ServletForDelete", value = "/ServletForDelete")
public class ServletForDelete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idString = request.getParameter("id");

        Image image = new Image();
        image.setImageId(Integer.parseInt(idString.trim()));
        ImageService imageService=new ImageServiceImp();
        imageService.Delete(image);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
