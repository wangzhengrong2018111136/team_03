package edu.chinasoft.service;

import edu.chinasoft.pojo.Image;

import java.util.List;

public interface ImageService {
    void uploadFile(Image image);
    public void Delete(Image image);
    List<Image> findAllImage();
}
