package edu.chinasoft.service;

import edu.chinasoft.dao.ImageDao;
import edu.chinasoft.dao.ImageDaoImp;
import edu.chinasoft.pojo.Image;
import edu.chinasoft.tool.JDBCUtil;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class ImageServiceImp implements ImageService {
    private Logger logger = Logger.getLogger(ImageServiceImp.class);
    private ImageDao imageDao=new ImageDaoImp();

    @Override
    public  void   uploadFile(Image image){
        logger.debug("ImageServiceImp>>>uploadFile start ……");
        imageDao.insertTable(image);
        logger.debug("ImageServiceImp>>>uploadFile end ……");
    }
    @Override
    public List<Image> findAllImage(){
        logger.debug("ImageServiceImp>>>findAllImage start ……");
        List<Image> result = imageDao.selectAll();
        logger.debug("ImageServiceImp>>>findAllImage start ……");
        return result;
    }
    public  void Delete(Image image){
        logger.debug("ImageDaoImp>>>delelte  start ……");
        //  步骤2：获取数据库连接对象
        Connection connection= JDBCUtil.getConnection();
        //  步骤3：执行sql语句
        String sql="DELETE  FROM t_image WHERE t_image.`image_id`=?;";
        PreparedStatement ps=null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1,image.getImageId());
            int result = ps.executeUpdate();
            if(result>0){
                System.out.println("执行成功！");
            }else{
                System.out.println("执行失败！");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally{

            //  步骤4：关闭资源
            JDBCUtil.close(null,ps,connection);
        }
        logger.debug("ImageDaoImp>>>delelte  end ……");

    }


}
