package edu.chinasoft.pojo;

public class Image {
    private int imageId;
    private String imageName;
    private String imageDesc;
    private String imageUrl;


    public Image() {
    }

    public Image(String imageName, String imageDesc, String imageUrl) {
        this.imageName = imageName;
        this.imageDesc = imageDesc;
        this.imageUrl = imageUrl;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageDesc() {
        return imageDesc;
    }

    public void setImageDesc(String imageDesc) {
        this.imageDesc = imageDesc;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
