package edu.chinasoft.servlet;

import edu.chinasoft.pojo.Image;
import edu.chinasoft.service.ImageService;
import edu.chinasoft.service.ImageServiceImp;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "servletforfileupload", value = "/servlet/servletforfileupload")
public class ServletForFileupload extends HttpServlet {
    private Logger logger = Logger.getLogger(ServletForFileupload.class);
    private ImageService imageService=new ImageServiceImp();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.debug("ServletForFileupload>>>doPost start ……");
        /*
         * 创建请求对象解析器
         */
        DiskFileItemFactory diff =
                new DiskFileItemFactory();

        ServletFileUpload sfu =
                new ServletFileUpload(diff);
        sfu.setHeaderEncoding("utf-8");

        // 创建一个图片对象
        Image image = new Image();


        try {
            /*
         获取请求里的所有标签对象
         */
            List<FileItem> items =
                    sfu.parseRequest(request);
            for (int i = 0; i < items.size(); i++) {
                //  循环遍历标签对象
                FileItem item = items.get(i);
                if (!item.isFormField()) {                        //这是一个普通的表单域{                      //这是上传文件域
                    // 获取所要上传文件的名字
                    String fileName = item.getName();
                    long l = System.currentTimeMillis();

                    String[] split = fileName.split("\\.");
                    fileName=split[0]+"_"+l+"."+split[1];

                    image.setImageName(fileName);


                    //  获取相对路径的绝对路径
                    ServletContext servletContext = this.getServletContext();
                    String filePath = servletContext.getRealPath("/upload");
                    File fileFather = new File(filePath);
                    if (!fileFather.exists()) {
                        fileFather.mkdirs();
                    }

                    File file = new File(fileFather, fileName);

                    image.setImageUrl(""+request.getContextPath()+"/upload/"+fileName);
                    item.write(file);
                }
            }


        } catch (FileUploadException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


        // 通过调用服务器层方法，把图片信息插入到表
        imageService.uploadFile(image);







        logger.debug("ServletForFileupload>>>doPost start ……");
    }
}
