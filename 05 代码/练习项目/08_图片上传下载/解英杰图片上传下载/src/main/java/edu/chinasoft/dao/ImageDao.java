package edu.chinasoft.dao;

import edu.chinasoft.pojo.Image;

import java.util.List;

public interface ImageDao {
    void insertTable(Image image);

    List<Image> selectAll();
}
