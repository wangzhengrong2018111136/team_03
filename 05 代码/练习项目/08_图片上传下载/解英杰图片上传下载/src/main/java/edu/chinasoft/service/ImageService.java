package edu.chinasoft.service;

import edu.chinasoft.pojo.Image;

import java.util.List;

public interface ImageService {
    void uploadFile(Image image);

    List<Image> findAllImage();
}
