package edu.chinasoft.dao;

import edu.chinasoft.pojo.Image;
import edu.chinasoft.tool.JDBCUtil;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ImageDaoImp implements ImageDao {
    private Logger logger = Logger.getLogger(ImageDaoImp.class);


    @Override
    public void insertTable(Image image) {

        logger.debug("ImageDaoImp>>>insert  start ……");


        //  Java  通过jdbc驱动操作sql语句来实现
        //  步骤1：加载驱动

        //  步骤2：获取数据库连接对象
        Connection connection = JDBCUtil.getConnection();
        //  步骤3：执行sql语句
        String sql = "INSERT INTO t_image(t_image.`image_name`,t_image.`image_desc`,t_image.`image_url`) VALUES(?,?,?);";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, image.getImageName());
            ps.setString(2, image.getImageDesc());
            ps.setString(3, image.getImageUrl());


            int result = ps.executeUpdate();
            if (result > 0) {
                System.out.println("执行成功！");
            } else {
                System.out.println("执行失败！");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {

            //  步骤4：关闭资源
            JDBCUtil.close(null, ps, connection);
        }
        logger.debug("ImageDaoImp>>>insert  end ……");

    }

    @Override
    public List<Image> selectAll() {
        logger.debug("ImageDaoImp>>>selectAll  start ……");

        List<Image> result = new ArrayList<>();

        Connection connection = JDBCUtil.getConnection();
        String sql = "SELECT t_image.`image_id`,t_image.`image_name`,t_image.`image_url` FROM t_image;";
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            ps = connection.prepareStatement(sql);
            //  ResultSet相当于一个容器，该容器里的元素为表里的记录
            resultSet = ps.executeQuery();
            while (resultSet.next()) {
                int imageId = resultSet.getInt("image_id");
                String imageName = resultSet.getString("image_name");
                String imageUrl = resultSet.getString("image_url");

                Image image = new Image();
                image.setImageId(imageId);
                image.setImageName(imageName);
                image.setImageUrl(imageUrl);


                result.add(image);


            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.close(resultSet, ps, connection);
        }


        logger.debug("ImageDaoImp>>>selectAll  end ……");

        return result;

    }


}
