package edu.chinasoft.servlet;

import edu.chinasoft.pojo.Image;
import edu.chinasoft.service.ImageService;
import edu.chinasoft.service.ImageServiceImp;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "servletforfind", value = "/servlet/servletforfind")
public class ServletForFind extends HttpServlet {
    private Logger logger = Logger.getLogger(ServletForFind.class);
    private ImageService  imageService =new ImageServiceImp();


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForFind>>>doGet start ……");
        // 查询到所有的图片
        List<Image> allImage = imageService.findAllImage();
        // 把图片保存到容器里
        HttpSession session = request.getSession();
        session.setAttribute("allimage",allImage);
        logger.debug("ServletForFind>>>doGet end ……");

        // 跳转到相应页面
        response.sendRedirect(""+request.getContextPath()+"/findimage.jsp");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
