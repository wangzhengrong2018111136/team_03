package edu.chinasoft.dao;

import edu.chinasoft.pojo.Image;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.List;

public class ImageDaoImpTest extends TestCase {

    @Test
    public void testInsertTable() {
        ImageDao imageDaoImp = new ImageDaoImp();
        Image image = new Image();
        image.setImageName("image02");
        image.setImageDesc("image02_desc");
        image.setImageUrl("/upload/image02.jpg");
        imageDaoImp.insertTable(image);
    }
    @Test
    public void testSelectAll(){
        ImageDaoImp imageDaoImp = new ImageDaoImp();
        List<Image> images = imageDaoImp.selectAll();
        System.out.println(images);


    }
}