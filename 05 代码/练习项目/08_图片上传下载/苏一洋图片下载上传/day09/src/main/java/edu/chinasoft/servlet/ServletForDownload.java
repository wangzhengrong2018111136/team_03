package edu.chinasoft.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@WebServlet(name = "ServletForDownload", value = "/ServletForDownload")
public class ServletForDownload extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("content-type","image/jpg");
        response.setHeader("Content-Disposition","attachment;filename=image01.jpg");ServletContext servletContext = this.getServletContext();
        String imagePath = servletContext.getRealPath("/WEB-INF/classes/upload/image01.jpg");

        InputStream fin = new FileInputStream(imagePath);
        int lenght = -1;
        byte[] buff = new byte[1024];
        OutputStream fout = response.getOutputStream();

        while ((lenght = fin.read(buff)) != -1) {

            fout.write(buff, 0, lenght);

        }


        fin.close();
        fout.close();


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
