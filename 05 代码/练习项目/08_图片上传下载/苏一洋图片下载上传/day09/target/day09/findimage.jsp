<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/5/20
  Time: 10:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page pageEncoding="utf-8"   isELIgnored="false" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>显示所有图片</title>
</head>
<body>

<table align="center" width="60%" border="1px" cellpadding="0" cellspacing="0">

    <tr align="center">
        <c:forEach var="imageelement" items="${allimage}">
            <td> <img src="${imageelement.imageUrl}" width="200px"></td>
        </c:forEach>
    </tr>
    <tr align="center">
        <c:forEach var="imageelement" items="${allimage}">
            <td> ${imageelement.imageName} </td>
        </c:forEach>
    </tr>

</table>

</body>
</html>
