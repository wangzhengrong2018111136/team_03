package chinasoft.service;


import chinasoft.pojo.Student;
import chinasoft.pojo.StudentPage;

import java.util.List;

public interface StudentService {
    // 把学生信息写入到数据库
    void register(Student student);


    StudentPage findAllStudent(int pageNO);

    void  deleteStudentById(Student student);

    Student  findStudentById(Student student);

    void  modifyStudent(Student student);
}
