package user.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import user.pojo.User;
import user.pojo.UserPage;
import user.service.UserService;
import user.service.UserServiceImp;

@WebServlet(name = "servletforUserselect", value = "/servlet/servletforUserselect")
public class ServletForUserSelect extends HttpServlet {
    private UserService UserService = new UserServiceImp();
    private Logger logger = Logger.getLogger(ServletForUserSelect.class);


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForUserSelect>>>doGet  start……");


        // 获取当前页号
        String pagenoString = request.getParameter("pageno");

        int  pageno=pagenoString==null?1:Integer.parseInt(pagenoString);



        //步骤1：获取所有的学生信息
        UserPage allUser = UserService.findAllUser(pageno);


        //步骤2：把学生信息保存到容器里   session
        HttpSession session = request.getSession();
        session.setAttribute("Userpage", allUser);

        logger.debug("ServletForUserSelect>>>doGet  end……");
        //步骤3：跳转到相应的页面，然后在页面中显示学生信息
        response.sendRedirect("/User_war_exploded/showuser.jsp");


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}




