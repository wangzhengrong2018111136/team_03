package user.dao;
import org.apache.log4j.Logger;
import user.pojo.User;
import user.pojo.UserPage;
import user.tool.Constant;
import user.tool.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImp implements UserDao{
    private Logger logger = Logger.getLogger(UserDaoImp.class);
// 关于数据库表user crud操作    jdbc驱动

    // 向数据库表t_user插入一条记录
    public  void insert(User user){
        logger.debug("UserDaoImp>>>insert  start ……");


        //  Java  通过jdbc驱动操作sql语句来实现
        //  步骤1：加载驱动

        //  步骤2：获取数据库连接对象
        Connection connection= JDBCUtil.getConnection();
        //  步骤3：执行sql语句
        String sql="INSERT INTO t_user(t_user.user_id,t_user.`user_name`,t_user.`user_sex`) VALUES(?,?,?);";
        PreparedStatement ps=null;
        try {
            ps = connection.prepareStatement(sql);

            ps.setInt(1,user.getUserId());
            ps.setString(2,user.getUserName());
            ps.setString(3,user.getUserSex());
            int result = ps.executeUpdate();
            if(result>0){
                System.out.println("执行成功！");
            }else{
                System.out.println("执行失败！");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally{

            //  步骤4：关闭资源
            JDBCUtil.close(null,ps,connection);
        }
        logger.debug("UserDaoImp>>>insert  end ……");

    }
    // 更新t_user表里的记录

    public  void update(User user){
        logger.debug("UserDaoImp>>>update  start ……");
        //  步骤2：获取数据库连接对象
        Connection connection= JDBCUtil.getConnection();
        //  步骤3：执行sql语句
        String sql="UPDATE t_user SET t_user.`user_name`=?,t_user.`user_sex`=? WHERE  t_user.`user_id`=?";
        PreparedStatement ps=null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1,user.getUserName());
            ps.setString(2,user.getUserSex());
            ps.setInt(3,user.getUserId());
            int result = ps.executeUpdate();
            if(result>0){
                System.out.println("执行成功！");
            }else{
                System.out.println("执行失败！");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally{

            //  步骤4：关闭资源
            JDBCUtil.close(null,ps,connection);
        }
        logger.debug("UserDaoImp>>>update  end ……");

    }
    // 更新t_user表里的记录

    public  void delelte(User user){
        logger.debug("UserDaoImp>>>delelte  start ……");
        //  步骤2：获取数据库连接对象
        Connection connection= JDBCUtil.getConnection();
        //  步骤3：执行sql语句
        String sql="DELETE  FROM t_user WHERE t_user.`user_id`=?;";
        PreparedStatement ps=null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1,user.getUserId());
            int result = ps.executeUpdate();
            if(result>0){
                System.out.println("执行成功！");
            }else{
                System.out.println("执行失败！");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally{

            //  步骤4：关闭资源
            JDBCUtil.close(null,ps,connection);
        }
        logger.debug("UserDaoImp>>>delelte  end ……");

    }

    // 获取表t_user里的记录

    public List<User> select() {
        logger.debug("UserDaoImp>>>select  start ……");
        List<User> result = new ArrayList<>();


        Connection connection = JDBCUtil.getConnection();
        String sql = "SELECT t_user.`user_id`,t_user.`user_name`,t_user.`user_sex` FROM t_user";
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            ps = connection.prepareStatement(sql);
            //  ResultSet相当于一个容器，该容器里的元素为表里的记录
            resultSet = ps.executeQuery();
            while (resultSet.next()) {
                int userId = resultSet.getInt("user_id");
                String userName = resultSet.getString("user_name");
                String userSex = resultSet.getString("user_sex");

                User user = new User(userId, userName, userSex);
                result.add(user);

                System.out.println(userId + ":" + userName + ":" + userSex);

            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.close(resultSet, ps, connection);
        }

        logger.debug("UserDaoImp>>>select  end ……");
        return result;
    }

    public  User  selectUserById(User user) {
        logger.debug("UserDaoImp>>>selectUserById  start ……");
        User result = new User();
        Connection connection = JDBCUtil.getConnection();
        String sql="SELECT t_user.`user_id`,t_user.`user_name`,t_user.`user_sex`\n" +
                "FROM t_user\n" +
                "WHERE t_user.`user_id`=?;";
        PreparedStatement preparedStatement=null;
        ResultSet resultSet=null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,user.getUserId());
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                int userId = resultSet.getInt("user_id");
                String userName = resultSet.getString("user_name");
                String userSex = resultSet.getString("user_sex");


                result.setUserId(userId);
                result.setUserName(userName);
                result.setUserSex(userSex);

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally{
            JDBCUtil.close(resultSet,preparedStatement,connection);
        }


        logger.debug("UserDaoImp>>>selectUserById  end ……");

        return result;
    }
    public void init_recordNum(UserPage userPage) {
        logger.debug("UserDaoImp>>>init_recordNum  start ……");

        Connection connection = JDBCUtil.getConnection();
        String sql = "SELECT COUNT(*) recordnum   FROM t_user";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int recordnum = resultSet.getInt("recordnum");
                userPage.setRecordNum(recordnum);

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.close(resultSet, preparedStatement, connection);
        }


        logger.debug("UserDaoImp>>>init_recordNum  end ……");


    }



    public UserPage selectPage(int pageNO){
        logger.debug("UserDaoImp>>>selectPage  start ……");
        UserPage userPage = new UserPage();
        userPage.setPageNo(pageNO);
        userPage.setPageUnit(Constant.PAGE_UNIT);
        init_recordNum(userPage);

        // 初始化userList变量
        Connection connection = JDBCUtil.getConnection();
        String sql = "SELECT  t_user.`user_id`,t_user.`user_name`,t_user.`user_sex` FROM `t_user` LIMIT ?,?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, (pageNO-1)*Constant.PAGE_UNIT);
            preparedStatement.setInt(2, Constant.PAGE_UNIT);

            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int userId = resultSet.getInt("user_id");
                String userName = resultSet.getString("user_name");
                String userSex = resultSet.getString("user_sex");
                User user = new User();


                user.setUserId(userId);
                user.setUserName(userName);
                user.setUserSex(userSex);

                userPage.getUserList().add(user);

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.close(resultSet, preparedStatement, connection);
        }


        logger.debug("UserDaoImp>>>selectPage  end ……");
        return  userPage;
    }


}
