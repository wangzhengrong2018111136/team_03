package user.pojo;

import java.util.ArrayList;
import java.util.List;

public class UserPage {
    // 分页单位
    private int pageUnit;
    // 当前页
    private int pageNo;

    // 总记录数
    private int recordNum;

    // 总页数
    private int pageNum;


    //保存当前页面需要展示的学生信息
    private List<User> UserList=new ArrayList<>();


    public int getPageUnit() {
        return pageUnit;
    }

    public void setPageUnit(int pageUnit) {
        this.pageUnit = pageUnit;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordNum() {
        return recordNum;
    }

    public void setRecordNum(int recordNum) {
        this.recordNum = recordNum;
    }

    public int getPageNum() {
        return  (int)Math.ceil(this.recordNum*1.0/this.pageUnit) ;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }


    public List<User> getUserList() {
        return UserList;
    }

    public void setUserList(List<User> UserList) {
        this.UserList = UserList;
    }



    public  int  getPreNo(){
        int result=this.pageNo-1;
        if(result<=0){
            result=1;
        }
        return result;
    }

    public int getNextNO(){
        int result=this.getPageNo()+1;
        if(result>=this.getPageNum()){
            result=this.getPageNum();
        }
        return result;
    }
}

