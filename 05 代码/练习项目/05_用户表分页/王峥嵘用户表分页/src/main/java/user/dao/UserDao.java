package user.dao;

import user.pojo.User;
import user.pojo.UserPage;

import java.util.List;

public interface UserDao {
    void insert(User user);

    // 更新t_user表里的记录
    void update(User user);

    // 更新t_user表里的记录
    void delelte(User user);

    // 获取表t_user里的记录
    List<User> select();
    User  selectUserById(User user);
    UserPage selectPage(int pageNO);
}
