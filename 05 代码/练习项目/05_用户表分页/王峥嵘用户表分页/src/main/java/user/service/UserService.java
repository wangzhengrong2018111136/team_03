package user.service;

import user.pojo.User;
import user.pojo.UserPage;

import java.util.List;

public interface UserService {
    void register(User user);
    UserPage findAllUser(int pageNO);
    void  deleteUserById(User user);

    User  findUserById(User user);
    void  modifyUser(User user);
}
