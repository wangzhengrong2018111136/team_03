package user.servlet;


import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import org.apache.log4j.Logger;
import user.pojo.User;
import user.service.UserService;
import user.service.UserServiceImp;


@WebServlet(name = "servletforuserdelete", value = "/servlet/servletforuserdelete")
public class ServletForUserDelete extends HttpServlet {
    private Logger logger = Logger.getLogger(ServletForUserDelete.class);
    private UserService userService=new UserServiceImp();


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForUserDelete>>>doGet  start ……");
        // 获取删除的id
        String idString = request.getParameter("id");


        // 调用服务层方法删除
        User user = new User();
        user.setUserId(Integer.parseInt(idString));
        userService.deleteUserById(user);

        logger.debug("ServletForUserDelete>>>doGet  end ……");
        // 步骤3：跳转到显示用户页面
        response.sendRedirect("/User_war_exploded/servlet/servletforuserselect");



    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
