package user.pojo;

public class User {
    private  int  Id;
    private  String Name;
    private String Sex;

    public User() {
    }
    public User(int Id,String Name,String Sex){
        this.Id = Id;
        this.Name = Name;
        this.Sex = Sex;
    }
    public int getUserId() {
        return Id;
    }

    public void setUserId(int id) {
        Id = id;
    }

    public String getUserName() {
        return Name;
    }

    public void setUserName(String name) {
        Name = name;
    }

    public String getUserSex() {
        return Sex;
    }

    public void setUserSex(String sex) {
        Sex = sex;
    }
}
