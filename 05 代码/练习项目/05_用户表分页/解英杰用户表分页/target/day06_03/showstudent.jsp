<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/5/14
  Time: 10:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java"  pageEncoding="utf-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>显示学生信息</title>
</head>
<body>

<h3 align="center">学生信息</h3>
<table align="center" width="60%" border="1px" cellpadding="0px" cellspacing="0px">
    <tr align="center">
        <td>学生id</td>
        <td>学生姓名</td>
        <td>学生的性别</td>
        <td>操作</td>
    </tr>

    <c:forEach items="${studentpage.studentList}" var="studentelement">
        <tr align="center">
            <td>${studentelement.studentId}</td>
            <td>${studentelement.studentName}</td>
            <td>${studentelement.studentSex=='m'?'男':'女'}</td>
            <td>
                <a href="/day06_03/servlet/servletforstudentdelete?id=${studentelement.studentId}" onclick="return method01()">删除</a>
                |
                <a href="/day06_03/servlet/servletforstudentupdtepage?id=${studentelement.studentId}">更新</a></td>
        </tr>
    </c:forEach>

    <tr align="right">
        <td colspan="4">
            共${studentpage.pageNum}页 &nbsp;&nbsp;&nbsp;
            当前${studentpage.pageNo}页 &nbsp;&nbsp;&nbsp;

            <a href="/day06_03/servlet/servletforstudentselect?pageno=1">首页</a> &nbsp;&nbsp;&nbsp;

            <a href="/day06_03/servlet/servletforstudentselect?pageno=${studentpage.preNo}">上一页</a>  &nbsp;&nbsp;&nbsp;

            <c:forEach begin="1" end="${studentpage.pageNum}" step="1" var="index">

                <a href="/day06_03/servlet/servletforstudentselect?pageno=${index}">${index}</a>&nbsp;&nbsp;&nbsp;

            </c:forEach>
            <a href="/day06_03/servlet/servletforstudentselect?pageno=${studentpage.nextNO}">下一页</a>  &nbsp;&nbsp;&nbsp;

            <a href="/day06_03/servlet/servletforstudentselect?pageno=${studentpage.pageNum}">尾页</a> &nbsp;&nbsp;&nbsp;




        </td>

    </tr>



</table>


<script type="text/javascript">
    function method01(){
        var result=window.confirm("确定删除吗？");
        return result;
    }




</script>



</body>
</html>