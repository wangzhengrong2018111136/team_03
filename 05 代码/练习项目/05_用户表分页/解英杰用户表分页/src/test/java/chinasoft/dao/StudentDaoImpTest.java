package chinasoft.dao;

import chinasoft.pojo.Student;
import chinasoft.pojo.StudentPage;
import org.junit.Test;

import static org.junit.Assert.*;

public class StudentDaoImpTest {

    @Test
    public void testSelectStudentById() {
        StudentDao studentDaoImp = new StudentDaoImp();
        Student student = new Student();
        student.setStudentId(3);
        Student student1 = studentDaoImp.selectStudentById(student);
        System.out.println(student1.getStudentId()+":"+student1.getStudentName()+":"+student1.getStudentSex());
    }
    @Test
    public void testSelectPage() {
        StudentDao studentDaoImp = new StudentDaoImp();
        StudentPage studentPage = studentDaoImp.selectPage(3);
        System.out.println(studentPage);
    }
    @Test
    public void testSelectPage01() {

        double ceil = Math.ceil(9.0 / 4);
        System.out.println(ceil);
    }

}