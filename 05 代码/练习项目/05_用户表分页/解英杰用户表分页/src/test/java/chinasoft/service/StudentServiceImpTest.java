package chinasoft.service;

import chinasoft.pojo.Student;
import org.junit.Test;

import static org.junit.Assert.*;

public class StudentServiceImpTest {

    @Test
    public void testRegister() {
        StudentServiceImp studentServiceImp = new StudentServiceImp();
        Student student = new Student();
        student.setStudentName("cjgong01");
        student.setStudentSex("m");
        studentServiceImp.register(student);
    }
}