package edu.chinasoft.service;

import com.syy.pojo.Student;
import com.syy.service.StudentServiceImp;
import org.junit.Test;

public class StudentServiceImpTest {

    @Test
    public void testRegister() {
        StudentServiceImp studentServiceImp = new StudentServiceImp();
        Student student = new Student();
        student.setStudentName("cjgong01");
        student.setStudentSex("m");
        studentServiceImp.register(student);
    }
}
