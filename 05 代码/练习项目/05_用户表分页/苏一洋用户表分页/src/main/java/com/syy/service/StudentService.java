package com.syy.service;

import com.syy.pojo.Student;
import com.syy.pojo.StudentPage;

public interface StudentService {
    // 把学生信息写入到数据库
    void register(Student student);


    StudentPage findAllStudent(int pageNO);

    void  deleteStudentById(Student student);

    Student  findStudentById(Student student);

    void  modifyStudent(Student student);
}

