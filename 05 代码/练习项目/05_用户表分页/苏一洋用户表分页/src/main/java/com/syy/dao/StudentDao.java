package com.syy.dao;

import com.syy.pojo.Student;
import com.syy.pojo.StudentPage;

import java.util.List;

public interface StudentDao {
    // 向数据库表t_student插入一条记录
    void insert(Student student);

    // 更新t_student表里的记录
    void update(Student student);

    // 更新t_student表里的记录
    void delelte(Student student);

    // 获取表t_student里的记录
    List<Student> select();

    Student  selectStudentById(Student student);

    StudentPage selectPage(int pageNO);
}

