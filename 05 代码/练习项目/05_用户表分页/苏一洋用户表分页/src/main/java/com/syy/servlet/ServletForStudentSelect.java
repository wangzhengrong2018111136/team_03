package com.syy.servlet;

import com.syy.pojo.StudentPage;
import com.syy.service.StudentService;
import com.syy.service.StudentServiceImp;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "servletforstudentselect", value = "/servlet/servletforstudentselect")
public class ServletForStudentSelect extends HttpServlet {
    private StudentService studentService = new StudentServiceImp();
    private Logger logger = Logger.getLogger(ServletForStudentSelect.class);


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForStudentSelect>>>doGet  start……");


        // 获取当前页号
        String pagenoString = request.getParameter("pageno");

        int  pageno=pagenoString==null?1:Integer.parseInt(pagenoString);



        //步骤1：获取所有的学生信息
        StudentPage allStudent = studentService.findAllStudent(pageno);


        //步骤2：把学生信息保存到容器里   session
        HttpSession session = request.getSession();
        session.setAttribute("studentpage", allStudent);

        logger.debug("ServletForStudentSelect>>>doGet  end……");
        //步骤3：跳转到相应的页面，然后在页面中显示学生信息
        response.sendRedirect("/day06_03/showstudent.jsp");


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
