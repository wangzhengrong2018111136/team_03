package com.syy.servlet;

import com.syy.pojo.Student;
import com.syy.service.StudentService;
import com.syy.service.StudentServiceImp;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "servletforstudentdelete", value = "/servlet/servletforstudentdelete")
public class ServletForStudentDelete extends HttpServlet {
    private Logger logger = Logger.getLogger(ServletForStudentDelete.class);
    private StudentService studentService=new StudentServiceImp();


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForStudentDelete>>>doGet  start ……");
        // 步骤1：获取所要删除学生的id
        String idString = request.getParameter("id");


        // 步骤2：调用服务层方法把学生删除
        Student student = new Student();
        student.setStudentId(Integer.parseInt(idString));
        studentService.deleteStudentById(student);

        logger.debug("ServletForStudentDelete>>>doGet  end ……");
        // 步骤3：跳转到显示所有学生页面
        response.sendRedirect("/day06_03/servlet/servletforstudentselect");



    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

