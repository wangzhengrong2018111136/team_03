package edu.chinasoft.servlet;

import edu.chinasoft.pojo.Student;
import edu.chinasoft.service.StudentService;
import edu.chinasoft.service.StudentServiceImp;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "servletforstudentupdtepage", value = "/servlet/servletforstudentupdtepage")
public class ServletForStudentUpdtePage extends HttpServlet {
    private StudentService studentService=new StudentServiceImp();
    private Logger logger = Logger.getLogger(ServletForStudentUpdtePage.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForStudentUpdtePage>>>doGet start ……");

        //步骤1：获取所要更新的用户id
        String idString = request.getParameter("id");

        //步骤2： 调用服务层方法，获取所要修学生的信息
        Student student = new Student();
        student.setStudentId(Integer.parseInt(idString));
        Student studentResult = studentService.findStudentById(student);

        //步骤3：把学生信息保存达到容器里
        HttpSession session = request.getSession();
        session.setAttribute("studentupdate",studentResult);


        //步骤4：跳转到更新学生页面
        logger.debug("ServletForStudentUpdtePage>>>doGet end ……");

        response.sendRedirect("/day06_03/update.jsp");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
