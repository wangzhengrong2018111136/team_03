package edu.chinasoft.servlet;

import edu.chinasoft.pojo.Student;
import edu.chinasoft.service.StudentService;
import edu.chinasoft.service.StudentServiceImp;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "servletforstudentupdte", value = "/servlet/servletforstudentupdte")
public class ServletForStudentUpdte extends HttpServlet {
    private StudentService studentService=new StudentServiceImp();
    private  Logger logger = Logger.getLogger(ServletForStudentUpdte.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForStudentUpdte>>>doPost start ……");
        //步骤1：获取页面中需要修改信息
        String idString = request.getParameter("student_id");
        String studentName = request.getParameter("student_name");
        String studentSex = request.getParameter("student_sex");


        Student student = new Student();
        student.setStudentId(Integer.parseInt(idString));
        student.setStudentName(studentName);
        student.setStudentSex(studentSex);


        //步骤2：调用服务层方法实现用户信息修改
        studentService.modifyStudent(student);




        //步骤3：跳转到显示所有学生页面

        logger.debug("ServletForStudentUpdte>>>doPost end ……");
        response.sendRedirect("/day06_03/servlet/servletforstudentselect");

    }
}
