package edu.chinasoft.service;

import edu.chinasoft.dao.StudentDao;
import edu.chinasoft.dao.StudentDaoImp;
import edu.chinasoft.pojo.Student;
import edu.chinasoft.pojo.StudentPage;
import org.apache.log4j.Logger;

import java.util.List;


public class StudentServiceImp implements StudentService {
    private Logger logger = Logger.getLogger(StudentServiceImp.class);
    // 接口编程
    private StudentDao studentDao=new StudentDaoImp();

    // 把学生信息写入到数据库
    @Override
    public void register(Student student){
        logger.debug("StudentServiceImp>>>register start ……");
        studentDao.insert(student);
        logger.debug("StudentServiceImp>>>register end ……");
    }

    @Override
    public StudentPage findAllStudent(int pageNO){
        logger.debug("StudentServiceImp>>>findAllStudent start ……");
        StudentPage studentPage = studentDao.selectPage(pageNO);
        logger.debug("StudentServiceImp>>>findAllStudent end ……");
        return studentPage;
    }

    @Override
    public void  deleteStudentById(Student student){
        logger.debug("StudentServiceImp>>>deleteStudentById start ……");
        studentDao.delelte(student);
        logger.debug("StudentServiceImp>>>deleteStudentById end ……");

    }

    @Override
    public Student  findStudentById(Student student){
        logger.debug("StudentServiceImp>>>findStudentById start ……");
        Student result = studentDao.selectStudentById(student);
        logger.debug("StudentServiceImp>>>findStudentById end ……");
        return result;
    }

    @Override
    public void  modifyStudent(Student student){
        logger.debug("StudentServiceImp>>>modifyStudent start ……");
        studentDao.update(student);
        logger.debug("StudentServiceImp>>>modifyStudent end ……");

    }

}
