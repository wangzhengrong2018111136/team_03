package edu.chinasoft.servlet;

import edu.chinasoft.pojo.Student;
import edu.chinasoft.service.StudentService;
import edu.chinasoft.service.StudentServiceImp;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "ServletForStudentadd", value = "/servlet/servletforstudentadd")
public class ServletForStudentAdd extends HttpServlet {
    private Logger logger = Logger.getLogger(ServletForStudentAdd.class);
    private StudentService studentService=new StudentServiceImp();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("ServletForStudent>>>doPost start……");



        // 获取页面提交的数据
        String studentName = request.getParameter("student_name");
        String studentSex = request.getParameter("student_sex");
        logger.debug(studentName+":"+studentSex);
        // 封装到学生类
        Student student = new Student();
        student.setStudentName(studentName);
        student.setStudentSex(studentSex);
        // 调用service层的方法，把数据插入到数据库
        studentService.register(student);

        logger.debug("ServletForStudent>>>doPost end……");
        // 如果注册成功，跳转到注册成功页面
        response.sendRedirect("/day06_03/register_success.jsp");


    }
}
