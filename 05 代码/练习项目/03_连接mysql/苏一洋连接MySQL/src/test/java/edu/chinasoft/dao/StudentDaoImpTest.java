package edu.chinasoft.dao;

import com.syy.dao.StudentDao;
import com.syy.dao.StudentDaoImp;
import com.syy.pojo.Student;
import org.junit.Test;

import java.util.List;

public class StudentDaoImpTest {

    @Test
    public void testInsert() {
        StudentDao studentDaoImp = new StudentDaoImp();
        Student student = new Student();
        student.setStudentName("qianlong ");
        student.setStudentSex("m");
        studentDaoImp.insert(student);
    }
    @Test
    public void testUpdate() {
        StudentDao studentDaoImp = new StudentDaoImp();
        Student student = new Student();
        student.setStudentId(2);
        student.setStudentName("b");
        student.setStudentSex("m");
        studentDaoImp.update(student);
    }
    @Test
    public void testDelelte() {
        StudentDao studentDaoImp = new StudentDaoImp();
        Student student = new Student();
        student.setStudentId(2);
        studentDaoImp.delelte(student);
    }
    @Test
    public void testSelect() {
        StudentDao studentDaoImp = new StudentDaoImp();
        List<Student> select = studentDaoImp.select();
        System.out.println(select);

    }
}