package wzr.train.dao;

import wzr.train.pojo.Student;
import wzr.train.tool.JDBCUtil;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentDaoImp implements StudentDao {
    private Logger logger = Logger.getLogger(StudentDaoImp.class);
    // 关于数据库表student crud操作    jdbc驱动

    // 向数据库表t_student插入一条记录
    @Override
    public  void insert(Student student){
        logger.debug("StudentDaoImp>>>insert  start ……");


        //  Java  通过jdbc驱动操作sql语句来实现
        //  步骤1：加载驱动

        //  步骤2：获取数据库连接对象
        Connection connection= JDBCUtil.getConnection();
        //  步骤3：执行sql语句
        String sql="INSERT INTO t_student(t_student.`student_name`,t_student.`student_sex`) VALUES(?,?);";
        PreparedStatement ps=null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1,student.getStudentName());
            ps.setString(2,student.getStudentSex());
            int result = ps.executeUpdate();
            if(result>0){
                System.out.println("执行成功！");
            }else{
                System.out.println("执行失败！");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally{

            //  步骤4：关闭资源
            JDBCUtil.close(null,ps,connection);
        }
        logger.debug("StudentDaoImp>>>insert  end ……");

    }
    // 更新t_student表里的记录
    @Override
    public  void update(Student student){
        logger.debug("StudentDaoImp>>>update  start ……");
        //  步骤2：获取数据库连接对象
        Connection connection= JDBCUtil.getConnection();
        //  步骤3：执行sql语句
        String sql="UPDATE t_student SET t_student.`student_name`=?,t_student.`student_sex`=? WHERE  t_student.`student_id`=?";
        PreparedStatement ps=null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1,student.getStudentName());
            ps.setString(2,student.getStudentSex());
            ps.setInt(3,student.getStudentId());
            int result = ps.executeUpdate();
            if(result>0){
                System.out.println("执行成功！");
            }else{
                System.out.println("执行失败！");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally{

            //  步骤4：关闭资源
            JDBCUtil.close(null,ps,connection);
        }
        logger.debug("StudentDaoImp>>>update  end ……");

    }
    // 更新t_student表里的记录
    @Override
    public  void delelte(Student student){
        logger.debug("StudentDaoImp>>>delelte  start ……");
        //  步骤2：获取数据库连接对象
        Connection connection= JDBCUtil.getConnection();
        //  步骤3：执行sql语句
        String sql="DELETE  FROM t_student WHERE t_student.`student_id`=?;";
        PreparedStatement ps=null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1,student.getStudentId());
            int result = ps.executeUpdate();
            if(result>0){
                System.out.println("执行成功！");
            }else{
                System.out.println("执行失败！");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally{

            //  步骤4：关闭资源
            JDBCUtil.close(null,ps,connection);
        }
        logger.debug("StudentDaoImp>>>delelte  end ……");

    }

    // 获取表t_student里的记录
    @Override
    public List<Student> select(){
        logger.debug("StudentDaoImp>>>select  start ……");
        List<Student>  result=new ArrayList<>();


        Connection connection = JDBCUtil.getConnection();
        String sql="SELECT t_student.`student_id`,t_student.`student_name`,t_student.`student_sex` FROM t_student";
        PreparedStatement ps=null;
        ResultSet resultSet=null;
        try {
            ps = connection.prepareStatement(sql);
            //  ResultSet相当于一个容器，该容器里的元素为表里的记录
            resultSet = ps.executeQuery();
            while(resultSet.next()){
                int studentId = resultSet.getInt("student_id");
                String studentName = resultSet.getString("student_name");
                String studentSex = resultSet.getString("student_sex");

                Student student = new Student(studentId, studentName, studentSex);
                result.add(student);

                System.out.println(studentId+":"+studentName+":"+studentSex);

            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JDBCUtil.close(resultSet,ps,connection);
        }

        logger.debug("StudentDaoImp>>>select  end ……");
        return result;
    }




}
