package wzr.train.dao;

import wzr.train.pojo.Student;
import org.junit.Test;

import java.util.List;

public class StudentDaoImpTest {

    @Test
    public void testInsert() {
        StudentDao studentDaoImp = new StudentDaoImp();
        Student student = new Student();
        //student.setStudentId(5);
        student.setStudentName("wzr");
        student.setStudentSex("m");
        studentDaoImp.insert(student);
    }
    @Test
    public void testUpdate() {
        StudentDao studentDaoImp = new StudentDaoImp();
        Student student = new Student();
        student.setStudentId(5);
        student.setStudentName("wzrong");
        student.setStudentSex("m");
        studentDaoImp.update(student);
    }
//    @Test
//    public void testDelelte() {
//        StudentDao studentDaoImp = new StudentDaoImp();
//        Student student = new Student();
//        student.setStudentId(2);
//        studentDaoImp.delelte(student);
//    }
    // @Test
//     public void testSelect() {
//         StudentDao studentDaoImp = new StudentDaoImp();
//         List<Student> select = studentDaoImp.select();
//         System.out.println(select);
//
//     }
}