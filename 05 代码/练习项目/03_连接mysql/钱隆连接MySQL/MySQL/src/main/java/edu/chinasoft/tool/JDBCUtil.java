package edu.chinasoft.tool;

import java.sql.*;

public class JDBCUtil {

    static{
        //  步骤1：加载驱动
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    // 获取数据库连接对象
    public  static Connection getConnection(){
        Connection connection=null;
        try {
            connection= DriverManager.getConnection(Constant.URL, Constant.USER, Constant.PASSWORD);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }

    //  关闭资源方法
    public static  void  close(ResultSet resultSet, PreparedStatement preparedStatement,Connection connection){
        try {
            if(resultSet!=null){
                resultSet.close();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                if(preparedStatement!=null){
                    preparedStatement.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }finally {
                try {
                    if(connection!=null){
                        connection.close();
                    }
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }

    }
}
