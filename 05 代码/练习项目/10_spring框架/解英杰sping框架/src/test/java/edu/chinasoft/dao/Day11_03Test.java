package edu.chinasoft.dao;

import edu.chinasoft.di.People;
import edu.chinasoft.pojo.Pojo;
import edu.chinasoft.service.Service;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

public class Day11_03Test {
    @Test
    public void testMethod01(){
        ApplicationContext ac = new ClassPathXmlApplicationContext("classpath:applicationcontext.xml");
        Pojo pojo = ac.getBean("pojo", Pojo.class);
        System.out.println(pojo);
        Dao dao = ac.getBean("dao", Dao.class);
        System.out.println(dao);
        Service service = ac.getBean("service", Service.class);
        System.out.println(service);
    }
    @Test
    public void testMethod02(){
        ApplicationContext ac = new ClassPathXmlApplicationContext("classpath:applicationcontext.xml");
        People people = ac.getBean("people", People.class);
        System.out.println(people);

    }



}