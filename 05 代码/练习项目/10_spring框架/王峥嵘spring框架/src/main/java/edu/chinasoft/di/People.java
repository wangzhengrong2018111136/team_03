package edu.chinasoft.di;

import edu.chinasoft.dao.Dao;
import edu.chinasoft.pojo.Pojo;
import edu.chinasoft.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component(value="people")
public class People {
    private int id;
    private String name;
    private Pojo pojo;
    private Dao dao;
    @Autowired
    @Qualifier(value="service")
    private Service service;

    public int getId() {
        return id;
    }

    @Value(value = "1")
    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }
    @Value(value="wzr")
    public void setName(String name) {
        this.name = name;
    }

    public Pojo getPojo() {
        return pojo;
    }
    @Resource(name="pojo")
    public void setPojo(Pojo pojo) {
        this.pojo = pojo;
    }

    public Dao getDao() {
        return dao;
    }

    @Autowired
    @Qualifier(value="dao")
    public void setDao(Dao dao) {
        this.dao = dao;
    }
}
