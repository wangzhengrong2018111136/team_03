package com.etc.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Johnny
 * @category 用户控制器
 */
@Controller
@RequestMapping("/user")
public class UserController {

	private Logger logger = Logger.getLogger(UserController.class);
	
	@RequestMapping({"/welcome","/wel"})
	public String welcome(String username){
		System.out.println("welcome username : "+ username);
		logger.info("welcome username : "+ username);
		return "index";
	}
	
}
