package com.etc.controller.model;

import com.etc.controller.SecondController;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Johnny
 * @category 如果需要扫描多个包则在配置文件中使用，号分隔
 */
@Controller
public class OtherPackageController {

    private Logger logger = Logger.getLogger(OtherPackageController.class);

    @RequestMapping("/hi")
    public String welcome(String username) {
        System.out.println("welcome username : " + username);
        logger.info("welcome username : " + username);
        return "index";
    }

}
