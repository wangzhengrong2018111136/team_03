package com.etc.controller;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 * @Author Johnny
 * 第2个控制器的学习
 *
 */
public class Index2Controller extends AbstractController {


    @Override
    protected ModelAndView handleRequestInternal(javax.servlet.http.HttpServletRequest httpServletRequest, javax.servlet.http.HttpServletResponse httpServletResponse) throws Exception {
        System.out.println("已经进入了控制器2");
        return new ModelAndView("index2");
    }
}
