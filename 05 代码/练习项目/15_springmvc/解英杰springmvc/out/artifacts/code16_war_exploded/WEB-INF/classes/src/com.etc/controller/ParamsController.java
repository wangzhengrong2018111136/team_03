package com.etc.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Johnny
 * 参数传递控制器
 */
@Controller
public class ParamsController {

    private Logger logger = Logger.getLogger(IndexController.class);

    @RequestMapping("/welcome1")
    public String index1(String username){
        System.out.println("welcome1 " + username);
        logger.info("coming..." + username);
        return "index";
    }

    // 方式二
    @RequestMapping("/welcome2")
    public String index2(@RequestParam String username) {
        System.out.println("welcome2 " + username);
        logger.info("coming..." + username);
        return "index";
    }

    // 方式三
    @RequestMapping("/welcome3")
    public String index3(@RequestParam(value = "n", required = false) String username) {
        System.out.println("welcome3 " + username);
        logger.info("coming..." + username);
        return "index";
    }

    // 方式四
    @RequestMapping(value = "/welcome4", method = RequestMethod.GET, params = "username")
    public String index4(String username) {
        System.out.println("welcome4 " + username);
        logger.info("coming..." + username);
        return "index";
    }

}
