<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Hello,SpringMVC</h1>
	<h3>注意：在Model中增加模型数据，若不指定key，则默认使用对象的类型作为key, <font color="red">而且是小写</font></h3>
	<h3>username(key:"userName") --> ${username}</h3>
	<h3>username(key:string) --> ${string}</h3>
	<h3>username(key:user) --> ${user.userName}</h3>
</body>
</html>