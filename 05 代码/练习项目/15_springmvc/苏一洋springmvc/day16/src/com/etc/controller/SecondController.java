package com.etc.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author Johnny
 * 使用注解完成
 */
@Controller
public class SecondController {

    @RequestMapping("/second")
    public String secondMethod(){
        System.out.println("进入了，注解使用的控制器");
        return "login";
    }

}
