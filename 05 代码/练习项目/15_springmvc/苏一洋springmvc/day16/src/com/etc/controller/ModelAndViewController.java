package com.etc.controller;

import com.etc.po.User;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * @author Johnny
 * @category 控制器
 */
@Controller
public class ModelAndViewController {

	private Logger logger = Logger.getLogger(ModelAndViewController.class);

	@RequestMapping("/mv")
	public ModelAndView index(String username) {
		logger.info("username = "+username);
		ModelAndView modelView = new ModelAndView();
		modelView.addObject("username", username);
		modelView.setViewName("index2");
		return modelView;
	}

	
	@RequestMapping("/model")
	public String index2(String username,Model model){
		logger.info("welcome username : " + username);
		User user = new User();
		user.setUserName(username);
		/**
		 * 默认使用对象的类型作为key：
		 * model.addAttribute("string", username)
		 * model.addAttribute("user", new User())
		 */
		model.addAttribute(username);
		model.addAttribute(user);
		return "index2";
	}
	
	@RequestMapping("/map")
	public String index3(String username,Map<String, Object> models){
		logger.info("welcome username : " + username);
		models.put("username", username);
		return "index2";
	}

}
