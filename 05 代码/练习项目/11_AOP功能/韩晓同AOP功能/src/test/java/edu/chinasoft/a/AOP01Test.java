package edu.chinasoft.a;

import org.junit.Test;

import static org.junit.Assert.*;

public class AOP01Test {

    @Test
    public void testMethod01() {
        YaoZhang xiaoMing = new XiaoMing();
        xiaoMing.yaoZhang();
    }
    @Test
    public void testMethod02() {
        XiaoMing xiaoMing = new XiaoMing();
        XiaoWu xiaoWu = new XiaoWu();
        YaoZhang company = new Company(xiaoMing, xiaoWu);
        company.yaoZhang();


    }
}