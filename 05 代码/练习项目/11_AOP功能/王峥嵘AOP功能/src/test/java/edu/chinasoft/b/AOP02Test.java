package edu.chinasoft.b;

import org.junit.Test;

import static org.junit.Assert.*;

public class AOP02Test {

    @Test
    public void testMethod01() {
        YaoZhang xiaoMing = new XiaoMing();
        xiaoMing.yaoZhang();

    }
    @Test
    public void testMethod02() {
        // 步骤1：通过要账公司工厂创建一个要账公司类
        XiaoMing xiaoMing = new XiaoMing();
        XiaoWu xiaoWu = new XiaoWu();
        CompanyFactory companyFactory = new CompanyFactory(xiaoMing, xiaoWu);
        YaoZhang company = companyFactory.createCompany();

        // 步骤2： 通过要账公司去要账
        company.yaoZhang();

    }
}