package edu.chinasoft.c;


import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;


public class CompanyFactory {
    private XiaoMing xiaoMing;
    private XiaoWu xiaoWu;

    public CompanyFactory(XiaoMing xiaoMing, XiaoWu xiaoWu) {
        this.xiaoMing = xiaoMing;
        this.xiaoWu = xiaoWu;
    }

    public XiaoMing createCompany(){
        // 通过cglib来创建

        // 创建代理类对象
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(xiaoMing.getClass());
        // 设置代理类如何来扩展目标类
        enhancer.setCallback((MethodInterceptor) (o, method, objects, methodProxy) -> {
            //拦截器 - 前置处理
            this.xiaoWu.before();
            Object result = method.invoke(this.xiaoMing, objects);
            //拦截器 - 后置处理
            this.xiaoWu.after();
            return result;
        });
        XiaoMing result = (XiaoMing)enhancer.create();


        return result;
    }


}
