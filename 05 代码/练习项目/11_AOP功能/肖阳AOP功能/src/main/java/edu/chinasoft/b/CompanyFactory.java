package edu.chinasoft.b;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class CompanyFactory {
    private YaoZhang xiaoMing;
    private XiaoWu xiaoWu;

    public CompanyFactory(YaoZhang xiaoMing, XiaoWu xiaoWu) {
        this.xiaoMing = xiaoMing;
        this.xiaoWu = xiaoWu;
    }

    public YaoZhang  createCompany(){
        /*
        Proxy类可以创建xiaoMing的代理类
           newProxyInstance方法
               第一个参数：设置xiaoMing的字节码对象
               第二个参数：设置xiaoMIng的接口类对象
               第三个参数：设置一个InvocationHandler类型的对象（需要设置代理类如何来扩展目标类）





         */

        YaoZhang result = (YaoZhang)Proxy.newProxyInstance(xiaoMing.getClass().getClassLoader(),
                xiaoMing.getClass().getInterfaces(),
                new InvocationHandler() {
                    //设置代理类如何类扩展目标类
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        CompanyFactory.this.xiaoWu.before();
                        // 调用小明类的相应方法
                        Object result = method.invoke(CompanyFactory.this.xiaoMing, args);
                        CompanyFactory.this.xiaoWu.after();
                        return result;
                    }
                });
        return result;
    }


}
