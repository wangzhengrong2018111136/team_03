package edu.chinasoft.e;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationcontext-02.xml")
public class AOP05Test {

    @Autowired
    @Qualifier(value="xiaoMing01")
    private XiaoMing xiaoMing;

    @Test
    public void yaoZhang() {
        xiaoMing.yaoZhang();
    }
}