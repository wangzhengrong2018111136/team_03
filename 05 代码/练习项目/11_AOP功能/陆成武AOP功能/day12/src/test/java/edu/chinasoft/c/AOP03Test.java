package edu.chinasoft.c;


import org.junit.Test;

import static org.junit.Assert.*;

public class AOP03Test {

    @Test
    public void  testMethod01(){
        XiaoMing xiaoMing = new XiaoMing();
        xiaoMing.yaoZhang();
    }
    @Test
    public void testMethod02(){
        XiaoMing xiaoMing = new XiaoMing();
        XiaoWu xiaoWu = new XiaoWu();

        CompanyFactory companyFactory = new CompanyFactory(xiaoMing, xiaoWu);
        XiaoMing company = companyFactory.createCompany();

        company.yaoZhang();
    }

}