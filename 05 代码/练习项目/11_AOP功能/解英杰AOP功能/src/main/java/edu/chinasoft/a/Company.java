package edu.chinasoft.a;

public class Company implements YaoZhang{
    private YaoZhang xiaoMing;
    private XiaoWu xiaoWu;

    public Company(YaoZhang xiaoMing, XiaoWu xiaoWu) {
        this.xiaoMing = xiaoMing;
        this.xiaoWu = xiaoWu;
    }

    @Override
    public void yaoZhang() {
        this.xiaoWu.before();
        this.xiaoMing.yaoZhang();
        this.xiaoWu.after();

    }
}
