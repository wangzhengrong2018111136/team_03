package com.dgxin.project.plant;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Plants extends Frame{
    //初始化天体坐标位置
    double x , x1 = 00,x2 = 200,x3 = 200,x4 = 200,x5 = 200,x6 = 200,x7 = 200,x8 = 200;
    double y , y1 = 00,y2 = 200,y3 = 200,y4 = 200,y5 = 200,y6 = 200,y7 = 200,y8 = 200;
    double t = 0,t1 = 0,t2 = 0,t3 = 0,t4 = 0,t5 = 0,t6 = 0,t7 = 0,t8 = 0;

    private Image Screen = null;

    void frame(){//设置程序主题窗口
        this.setSize(900,800);			//窗口大小code
        this.setLocation(250, 0);		//设置程序窗口位于界面位置
        this.setTitle("天体运行模拟");
        colse();					//调用方法，关闭程序窗口
        this.setVisible(true);			//显示程序主题窗口
        this.setBackground(Color.black);//设置背景颜色
        new PaintThread().start();		//启动动态运行机制
    }

//给repaint调用用于绘制某时刻窗口
    public void update(Graphics ghs) {	//控制屏幕不会闪动
        if(Screen == null)
        {
            Screen = this.createImage(900, 800);//建立窗口
        }
        Graphics g = Screen.getGraphics();
        Color oldColor = g.getColor();
        g.setColor(Color.black);
        g.fillRect(0, 0, 900, 900);
        g.setColor(oldColor);
        paint(g);//调用paint，画出某时刻的状态
        ghs.drawImage(Screen, 0, 0, null);
    }
    //导入图片
    Image sun = Toolkit.getDefaultToolkit().getImage("C:/training/sum/src/main/resources/images/sun.jpg");//资源图片位置
    Image background = Toolkit.getDefaultToolkit().getImage("C:\\training\\sum\\src\\main\\resources\\images\\background.jpg");
    Image mercury = Toolkit.getDefaultToolkit().getImage("C:/training/sum/src/main/resources/images/Mercury.jpg");//水星 87.9693
    Image venus = Toolkit.getDefaultToolkit().getImage("C:/training/sum/src/main/resources/images/Venus.jpg");//金星 224.71
    Image earth = Toolkit.getDefaultToolkit().getImage("C:\\training\\sum\\src\\main\\resources\\images\\earth.jpg");
    Image mars = Toolkit.getDefaultToolkit().getImage("C:/training/sum/src/main/resources/images/Mars.jpg");//火星 686.98
    Image jupiter = Toolkit.getDefaultToolkit().getImage("C:/training/sum/src/main/resources/images/Jupiter.jpg");//木星4263.2
    Image saturn = Toolkit.getDefaultToolkit().getImage("C:/training/sum/src/main/resources/images/Saturn.jpg");//土星 10759.5
    Image uranus = Toolkit.getDefaultToolkit().getImage("C:/training/sum/src/main/resources/images/Uranus.jpg");//天王星 30799.095
    Image neptune = Toolkit.getDefaultToolkit().getImage("C:/training/sum/src/main/resources/images/Neptune.jpg");//海王星 60152
    Image moon = Toolkit.getDefaultToolkit().getImage("C:/training/sum/src/main/resources/images/moon.jpg");//月亮 27.32

    public void paint(Graphics gh){
        gh.setColor(Color.gray);//设置轨道颜色
        //设置运行轨道
        gh.drawImage(background, 0 , 0 , null);
        gh.drawImage(sun , 435 , 435 , null);//画出太阳
        //画出各个天体的轨道
        gh.drawOval(401, 415, 88, 60);
        gh.drawOval(375, 395, 140, 100);
        gh.drawOval(335, 365, 220, 160);
        gh.drawOval(285, 335, 320, 220);
        gh.drawOval(155, 275, 580, 340);
        gh.drawOval(120, 245, 650, 400);
        gh.drawOval(80, 210, 730, 470);
        gh.drawOval(10, 165, 870, 560);


        //各大行星距太阳的距离依次是：水星、金星、地球、火星、木星、土星、天王星、海王星
        //根据各行星的公转周期，设置其运行速度

        //根据天体运行规律计算公转周期
        t1 = t1 + Math.PI / 87.9693;
        t2 = t2 + Math.PI / 224.71;
        t3 = t3 + Math.PI / 365;
        t = t + Math.PI / 27;		//设置月亮运动，按月亮自身的公转周期
        t4 = t4 + Math.PI / 686.98;
        t5 = t5 + Math.PI / 4263.2;
        t6 = t6 + Math.PI / 10759.5;
        t7 = t7 + Math.PI / 30799.095;
        t8 = t8 + Math.PI / 60152;


        //依据前一时刻位置在画出天体
        gh.drawImage(mercury , (int)x1 ,(int)y1, null);
        //利用天体周期，前一时刻位置计算下一时刻位置
        x1 = 440 + 44 * Math.cos(t1);
        y1 = 440 + 30 * Math.sin(t1);

        gh.drawImage(venus , (int)x2 ,(int)y2, null);
        x2 = 440 + 70 * Math.cos(t2);
        y2 = 440 + 50 * Math.sin(t2);
        gh.drawImage(earth , (int)x3 ,(int)y3, null);
        x3 = 440 + 110 * Math.cos(t3);
        y3 = 440 + 80 * Math.sin(t3);
        gh.drawImage(moon , (int)x ,(int)y, null);
        x = x3 + 15 * Math.cos(t);
        y = y3 + 14 * Math.sin(t);
        gh.drawImage(mars , (int)x4 ,(int)y4, null);
        x4 = 443 + 160 * Math.cos(t4);
        y4 = 443 + 110 * Math.sin(t4);
        gh.drawImage(jupiter , (int)x5 ,(int)y5, null);
        x5 = 437 + 290 * Math.cos(t5);
        y5 = 437 + 170 * Math.sin(t5);
        gh.drawImage(saturn , (int)x6 ,(int)y6, null);
        x6 = 438 + 325 * Math.cos(t6);
        y6 = 438 + 200 * Math.sin(t6);
        gh.drawImage(uranus , (int)x7 ,(int)y7, null);
        x7 = 439 + 365 * Math.cos(t7);
        y7 = 439 + 235 * Math.sin(t7);
        gh.drawImage(neptune , (int)x8 ,(int)y8, null);
        x8 = 439 + 435 * Math.cos(t8);
        y8 = 439 + 280 * Math.sin(t8);
    }

    private class PaintThread extends Thread{		//创建星体运动
        public void run(){
            while(true){
                repaint();//每40毫秒重新画出天体位置
                try {
                    Thread.sleep(40);//线程睡眠40毫秒
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void colse()//窗口关闭事件
    {
        this.addWindowListener(new WindowAdapter() //创建窗口监听,关闭程序界面窗口
        {
            public void windowClosing(WindowEvent e)
            {

                System.exit(0);
            }
        });
    }


    public static void main(String[] args){
        Plants P = new Plants();
        P.frame();
    }

}