package edu.chinasoft.servlet;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "servlet06", value = "/servlet/servlet06")
public class Servlet06 extends HttpServlet {
    private Logger logger = Logger.getLogger(Servlet06.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("Servlet06>>>doGet start ……");
        logger.debug("Servlet06>>>doGet end ……");

        //获取一个包含资源对象
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/html02.html");
        //实现跳转到所包含的资源
        requestDispatcher.forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}