package edu.chinasoft.e;

import edu.chinasoft.d.Servlet06;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "servlet07", value = "/servlet/servlet07")
public class Servlet07 extends HttpServlet {
    private Logger logger = Logger.getLogger(Servlet07.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("Servlet07>>>doPost start ……");
        /*
         * 创建请求对象解析器
         */
        DiskFileItemFactory diff =
                new DiskFileItemFactory();

        ServletFileUpload sfu =
                new ServletFileUpload(diff);
        sfu.setHeaderEncoding("utf-8");


        try {
            /*
         获取请求里的所有标签对象
         */
            List<FileItem> items =
                    sfu.parseRequest(request);
            String  fileName="";
            for (int i = 0; i < items.size(); i++) {
                //  循环遍历标签对象
                FileItem item = items.get(i);
                if (item.isFormField()) {                        //这是一个普通的表单域
                    // 获取标签name属性的值
                    String paramName = item.getFieldName();
                    // 获取标签value属性的值
                    String paramValue = item.getString();
                    paramValue = new String(
                            paramValue.getBytes("iso-8859-1"), "utf-8");
                    fileName=paramValue;
                    logger.debug(paramName+":"+paramValue);
                }
            }



            for (int i = 0; i < items.size(); i++) {
                //  循环遍历标签对象
                FileItem item = items.get(i);
                if (!item.isFormField()) {                        //这是一个普通的表单域{                      //这是上传文件域
                    //  获取相对路径的绝对路径
                    ServletContext servletContext = this.getServletContext();
                    String filePath = servletContext.getRealPath("/upload");
                    File fileFather = new File(filePath);
                    if(!fileFather.exists()){
                        fileFather.mkdirs();
                    }

                    File file = new File(fileFather, fileName);
                    item.write(file);
                }
            }


        } catch (FileUploadException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }


        logger.debug("Servlet07>>>doPost end ……");
    }
}
