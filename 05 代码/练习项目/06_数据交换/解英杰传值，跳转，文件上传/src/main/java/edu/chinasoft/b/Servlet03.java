package edu.chinasoft.b;

import edu.chinasoft.a.Servlet02;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "servlet03", value = "/servlet/servlet03")
public class Servlet03 extends HttpServlet {
    private Logger logger = Logger.getLogger(Servlet03.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("Servlet03>>>doGet start……");
        /*
        请求头的key:  永远都是字符串类型
        请求头的value:
                     字符串类型    getHeader()
                     整数类型      getIntHeader()
                     时间类型      getDateHeader()
         */
        String value = request.getHeader("User-Agent");

        String result="";
        if(value.contains("Chrome")){
            result="谷歌浏览器";
        }else{
            result="非谷歌浏览器";
        }
        logger.debug("测试结果为："+result);

        logger.debug("Servlet03>>>doGet  end……");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
