package com.syy.d;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "Servlet05", value = "/Servlet05")
public class Servlet05 extends HttpServlet {
    private Logger logger = Logger.getLogger(Servlet05.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("Servlet05>>>doGet start ……");
        String contextPath = request.getContextPath();
        logger.debug("动态获取项目的名字："+contextPath);
        logger.debug("Servlet05>>>doGet end ……");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
