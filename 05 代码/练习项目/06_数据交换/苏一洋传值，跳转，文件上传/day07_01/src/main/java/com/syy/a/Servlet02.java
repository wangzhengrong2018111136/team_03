package com.syy.a;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "servlet02", value = "/servlet/servlet02")
public class Servlet02 extends HttpServlet {
    private Logger logger = Logger.getLogger(Servlet02.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.debug("Servlet02>>>doGet start ……");
        // 获取get请求里的数据
        // 如果key值有重复，默认获取到的是第一个value
//        String result01 = request.getParameter("key01");
//        logger.debug("key01:"+result01);

        // 如果key值有重复，需要通过下面方式获取
        String[] valueList = request.getParameterValues("key01");
        for(String element:valueList){
            logger.debug("key01:"+element);
        }
        logger.debug("Servlet02>>>doGet end ……");
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
