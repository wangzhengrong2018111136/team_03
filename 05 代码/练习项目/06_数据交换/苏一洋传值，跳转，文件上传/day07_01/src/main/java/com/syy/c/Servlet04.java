package com.syy.c;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "servlet04", value = "/servlet/servlet04")
public class Servlet04 extends HttpServlet {
    private Logger logger = Logger.getLogger(Servlet04.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("Servlet04>>>doGet start……");
        //获取请求行各个部分的数据
        
        // 获取请求方式
        String method = request.getMethod();
        
        // 获取请求路径
        String requestURI = request.getRequestURI();
        
        // 获取请求协议
        String scheme = request.getProtocol();

        logger.debug("请求方式："+method);
        logger.debug("请求路径："+requestURI);
        logger.debug("请求协议："+scheme);


        logger.debug("Servlet04>>>doGet end……");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
