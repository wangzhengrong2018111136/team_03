package edu.chinasoft.config;

import edu.chinasoft.handler.Servlet01;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Servlet;

@Configuration
public class MyConfig {
    //把方法的返回值注入到ioc容器里
    @Bean
    public ServletRegistrationBean<Servlet01> servlet01URL(){
        return new ServletRegistrationBean(new Servlet01(),"/servlet/servlet011");
    }

}
