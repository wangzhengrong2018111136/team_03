package edu.chinasoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
public class Springboot0202Application {

	public static void main(String[] args) {
		SpringApplication.run(Springboot0202Application.class, args);
	}

}
