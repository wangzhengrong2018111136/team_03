package edu.chinasoft.mapper;

import edu.chinasoft.pojo.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/*
@Repository+@MapperScan= @Mapper




 */


@Mapper
@Repository(value="studentMapper")
public interface StudentMapper {
    public void  insertTable01(Student student);

}
