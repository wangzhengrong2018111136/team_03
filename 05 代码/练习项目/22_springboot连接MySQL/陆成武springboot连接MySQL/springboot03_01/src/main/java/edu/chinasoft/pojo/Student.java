package edu.chinasoft.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@Alias("student")
public class Student {
    private int studentId;
    private String studentName;
    private String studentSex;

    public Student(String studentName, String studentSex) {
        this.studentName = studentName;
        this.studentSex = studentSex;
    }
}
