package edu.chinasoft.mapper;

import edu.chinasoft.pojo.Student;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class StudentMapperTest {

    @Autowired
    private DataSource dataSource;
    @Autowired
    @Qualifier("studentMapper")
    private StudentMapper studentMapper;

    @Test
    public  void   testMethod01(){
        System.out.println(dataSource);
        System.out.println(dataSource.getClass().getName());

    }

    @Test
    public  void   testMethod02(){
        System.out.println(studentMapper);
        Student student = new Student("cjgong04","m");
        studentMapper.insertTable01(student);

    }



}