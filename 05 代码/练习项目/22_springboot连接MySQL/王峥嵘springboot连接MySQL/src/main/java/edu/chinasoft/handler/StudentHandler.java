package edu.chinasoft.handler;

import edu.chinasoft.pojo.Student;
import edu.chinasoft.service.StudentService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Data
public class StudentHandler {
    @Autowired
    @Qualifier("studentservice")
    private StudentService studentService;

    @GetMapping("/method01")
    public String method01(){
        Student student = new Student("wzr","m");
        studentService.registerStuent(student);
        return "result:success";
    }
}
