package edu.chinasoft.service;

import edu.chinasoft.mapper.StudentMapper;
import edu.chinasoft.pojo.Student;
import jdk.nashorn.internal.runtime.logging.Logger;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Data
@Service("studentservice")
public class StudentServiceImp implements StudentService {
    @Autowired
    @Qualifier("studentMapper")
    private StudentMapper studentMapper;

    @Override
    public void  registerStuent(Student student){
        studentMapper.insertTable01(student);

    }


}
