package edu.chiansoft.dao;

import edu.chiansoft.pojo.Student;
import edu.chiansoft.util.MybatisUtil;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class StudentDaoImp implements  StudentDao{

    //  手动提交事务
//    @Override
//    public void insertTable(Student student) {
//        // 通过mybatis框架操作数据库里的表
//        // 步骤1：创建sqlsessionFactory
//        String resource = "mybatis-config.xml";
//        InputStream inputStream = null;
//        try {
//            inputStream = Resources.getResourceAsStream(resource);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
//        // 步骤2：创建sqlsession对象  (Connection)   需要手动提交事务
//        SqlSession sqlSession = sqlSessionFactory.openSession();
//
//        // 步骤3： 通过sqlsession执行相应的sql语句
//        sqlSession.insert("edu.chiansoft.dao.StudentDao.insertTable",student);
//        sqlSession.commit();    // 手动提交事务
//        sqlSession.close();
//
//    }

//    //  自动提交事务
//    @Override
//    public void insertTable(Student student) {
//        // 通过mybatis框架操作数据库里的表
//        // 步骤1：创建sqlsessionFactory
//        String resource = "mybatis-config.xml";
//        InputStream inputStream = null;
//        try {
//            inputStream = Resources.getResourceAsStream(resource);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
//        // 步骤2：创建sqlsession对象  (Connection)   需要手动提交事务
//        SqlSession sqlSession = sqlSessionFactory.openSession(true);
//
//        // 步骤3： 通过sqlsession执行相应的sql语句
//        sqlSession.insert("edu.chiansoft.dao.StudentDao.insertTable",student);
//
//        sqlSession.close();
//
//    }
    //  自动提交事务
    @Override
    public void insertTable(Student student) {

        // 步骤2：创建sqlsession对象  (Connection)   需要手动提交事务
        SqlSession sqlSession = MybatisUtil.getSqlSession(true);

        // 步骤3： 通过sqlsession执行相应的sql语句
        sqlSession.insert("edu.chiansoft.dao.StudentDao.insertTable",student);

        MybatisUtil.close(sqlSession);

    }
}
