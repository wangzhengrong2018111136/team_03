package edu.chiansoft.pojo;

public class Student {
    private  int studentId;
    private String studentName;
    private String studentSex;


    public Student() {
    }

    public Student(String studentName, String studentSex) {
        this.studentName = studentName;
        this.studentSex = studentSex;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentSex() {
        return studentSex;
    }

    public void setStudentSex(String studentSex) {
        this.studentSex = studentSex;
    }
}
