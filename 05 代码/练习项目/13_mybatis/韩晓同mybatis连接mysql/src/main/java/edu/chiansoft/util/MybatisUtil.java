package edu.chiansoft.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class MybatisUtil {

    public static SqlSessionFactory sqlSessionFactory;
    // 加载类需要执行得代码
    static {
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(Constant.RESOURCE);
        } catch (IOException e) {
            e.printStackTrace();
        }
      sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    public static SqlSession getSqlSession(boolean flag){
        SqlSession sqlSession = sqlSessionFactory.openSession(flag);
        return sqlSession;
    }

    public static  void  close(SqlSession sqlSession){
        if(sqlSession!=null){
            sqlSession.close();
        }
    }

}
