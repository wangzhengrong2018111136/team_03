package edu.chiansoft.mapper;

import edu.chinasoft.mapper.StudentMapper;
import edu.chinasoft.pojo.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationcontext.xml")
public class Mapper01Test {

    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @Autowired
    @Qualifier("studentMapper")
    private StudentMapper studentMapper;




    @Test
    public void testDateSource() {
        System.out.println(dataSource);
    }
    @Test
    public void testStudentMapper() {
        Student student = new Student();
        student.setStudentName("springplus");
        student.setStudentSex("m");
        studentMapper.insert(student);
    }


}