package edu.chiansoft.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.chiansoft.pojo.Student;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository(value="studentMapper")
public interface StudentMapper extends BaseMapper<Student> {

}
