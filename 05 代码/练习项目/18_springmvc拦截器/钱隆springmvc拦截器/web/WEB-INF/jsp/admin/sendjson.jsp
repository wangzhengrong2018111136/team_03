<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    request.setAttribute("basePath", basePath);
%>
<!DOCTYPE>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>发送JSON请求</title>
    <!-- 引入JQuery -->
    <script type="text/javascript" src="${basePath}/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript">
        function sendJson() {
            var name = $("#n").val();

            //使用JQuery的Ajax方式提交
            $.ajax({
                type: "post",
                url: "receive.action",
                contentType: 'application/json;charset=utf-8',
                data: name,
                success: function (data) {
                    $("#h").html(data);
                }
            });
        }

    </script>
</head>

<body>
<h1>sendjson.jsp</h1>
<input id="n" type="text"/>
<button onclick="sendJson()">确定</button>
<h3 id="h"></h3>
</body>
</html>