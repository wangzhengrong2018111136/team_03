package cn.etc.controller;

import java.io.File;
import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * @author Johnny
 * @category 用户上传示例
 */
@Controller
public class UploadController {

	@RequestMapping("/toUpload")
	public String toUpload() {
		System.out.println("进入了上传页面");
		return "admin/upload";
	}

	/**
	 * @RequestParam("file") 微软用户一定要加这个()里面写的名称要和参数名称一致
	 */
	@RequestMapping("/upload")
	public String upload(@RequestParam("file") CommonsMultipartFile file) {
		try {
			System.out.println(file.getOriginalFilename());
			String path = "D:"+ File.separator + file.getOriginalFilename();//加上分隔符\
			file.transferTo(new File(path));//将文件内容复制到指定位置
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "ok";
	}
	
}
