package cn.etc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.etc.po.User;

/**
 * @author Johnny Ajax控制器
 */
@Controller
public class AjaxController {

	@RequestMapping(value = "/byid", produces = "application/json;charset=utf-8")
	public @ResponseBody String findById(Integer id) {
		String str = "{\"id\":1,\"name\":'小婷',\"phone\":'adfsdf'}";
		return str;
	}

	@RequestMapping(value = "/byid2")
	public @ResponseBody User findById2(int id) {
		User user = new User();
		user.setId(id);
		user.setName("小霞");
		user.setPhone("188");
		// 这里可以使用JSON工具转换为字符串
		return user;
	}

	// 跳转到sendjson页面
	@RequestMapping("/send")
	public String sendjson() {
		System.out.println("coming sendjson method...");
		return "admin/sendjson";
	}
	
	@RequestMapping("/send1")
	public String sendjson1() {
		return "admin/sendjson1";
	}

	@RequestMapping("/receive")
	public @ResponseBody String receive(@RequestBody String name) {
		String str = "welcome " + name;
		System.out.println(str);
		return str;
	}

	// 跳转到sendjson2页面
	@RequestMapping("/send2")
	public String sendjson2() {
		return "admin/sendjson2";
	}

	@RequestMapping("/receive2")
	public @ResponseBody String receive2(@RequestBody User user) {
		String str =user.getName();
		System.out.println(str);
		return str;
	}
	

	@RequestMapping("/send3")
	public String sendjson3() {
		return "admin/sendjson3";
	}

	/*
	 * 中文乱码问题得到解决
	 * 原理: 手动给对应的Accept返回制定格式编码数据。
	 * 发现produces设置多个Accept只有第一个的charset是有用的,
	 */
	@RequestMapping(value = "/receive3", produces={"text/html;charset=UTF-8;","application/json;"})
	public @ResponseBody String receive3(@RequestBody String name) {
		String str = "welcome " + name;
		System.out.println(str);
		return str;
	}
}
