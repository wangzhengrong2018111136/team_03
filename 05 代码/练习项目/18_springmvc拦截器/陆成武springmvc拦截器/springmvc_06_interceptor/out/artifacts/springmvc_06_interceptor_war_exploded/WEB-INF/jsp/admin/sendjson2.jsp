<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    request.setAttribute("basePath", basePath);
%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>发送JSON请求</title>
<!-- 引入JQuery -->
<script type="text/javascript" src="${basePath}/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript">
function sendJson(){
	//创建对象
	var user={
			name:"ccg",
			age:22,
			phone:18898472345
	}
	// 把JS当中的对象转换成为字符串
	// 1、jquery.param name="小丽"&phone="110"&age=20
	// 2、json字符串
	var str = JSON.stringify(user);
	$.ajax({
		type:"post",
		url:"receive2.action",
		contentType:'application/json;charset=utf-8',
		data:str,
		success: function(data){
			console.log(data);
			$("#h").html(data);
	      }
	});
}
</script>
</head>

<body>
<pre>
var user={
        name:"ccg",
        age:22,
        phone:18898472345
}
</pre>
<h2>点击按钮，从页面中的JS获取User对象，将该对象用JSON.stringify(user)方式将用户转换成为字符串，然后传送给后台</h2>
<button onclick="sendJson()">确定</button>
<h3 id="h"></h3>
</body>
</html>