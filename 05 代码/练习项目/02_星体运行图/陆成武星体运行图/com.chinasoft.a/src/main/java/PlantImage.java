import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//  当前类是一个窗口
class PlantImage1 extends Frame {
    // 表示窗口的宽度和高度
    private int  width=900;
    private int  height=900;
    private Image Screen=null;

    public static void main(String[] args) {
        PlantImage1 plantImage = new PlantImage1();
        plantImage.init();
    }

    public  void init() {
        this.setSize(width,height);
        this.setLocation(200,200);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        ThreadInner threadInner = new ThreadInner();
        threadInner.start();
        this.setVisible(true);
        this.show();
    }


    class   ThreadInner extends Thread{

        @Override
        public void run() {
            while(true){
                PlantImage1.this.repaint();
            }
        }
    }

    // 如果通过多线程解决打开窗口不显示问题， 闪动
    public void update(Graphics ghs) {	//控制屏幕不会闪动
        if(Screen == null)
        {
            Screen = this.createImage(width, height);
        }
        Graphics g = Screen.getGraphics();
        Color oldColor = g.getColor();
        g.setColor(Color.black);
        g.fillRect(0, 0, width, height);
        g.setColor(oldColor);
        paint(g);
        ghs.drawImage(Screen, 0, 0, null);
    }

    @Override
    public void paint(Graphics g) {
        Color colorSrc = g.getColor();
        String pathImage="D:\\code\\com.chinasoft.a\\src\\main\\resources\\background.jpg";
        Image image = Toolkit.getDefaultToolkit().getImage(pathImage);
        g.drawImage(image,0,0,null);
    }
}