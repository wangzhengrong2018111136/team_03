package com.qianlong.PlantImage01;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//当前类是一个窗口程序
public class PlantImage extends Frame {
    //表示窗口的长宽高
    private int width = 900;
    private int height = 900;
    private Image Screen = null;


    public static void main(String[] args) {
        PlantImage plantImage = new PlantImage();
        plantImage.init();
    }

    //显示窗口
    public void init(){
        //显示窗口大小
        this.setSize(width,height);
        //设置窗口的显示位置
        this.setLocation(200,200);
        this.addWindowFocusListener(new WindowAdapter() {
            // 单击右上角的退出按钮
            @Override
            public void windowClosing(WindowEvent e) {
                //  关闭窗口代码
                System.exit(0);

            }
        });

        //为解决打开窗体程序后不显示图片，使用多线程
        //创建一个新线程
        ThreadInner threadInner = new  ThreadInner();
        threadInner.start();

        //显示窗口
        this.setVisible(true);
        this.show();
    }


    // 设计一个线程内部类，该类实现调用paint方法
    class ThreadInner extends Thread {
        //用线程的run方法执行画笔方法

        @Override
        public void run(){
            while (true){
                PlantImage.this.repaint();
            }
        }


    }
    //控制屏幕不会闪动
    public void update(Graphics ghs) {
        if(Screen == null)
        {
            Screen = this.createImage(width, height);
        }
        Graphics g = Screen.getGraphics();
        Color oldColor = g.getColor();
        g.setColor(Color.black);
        g.fillRect(0, 0, width, height);
        g.setColor(oldColor);
        paint(g);
        ghs.drawImage(Screen, 0, 0, null);
    }


    //Graphics  画笔
    @Override
    public void paint(Graphics g){
        //设置画笔颜色
        Color colorSrc = g.getColor();

        //绘制图片
        //获取图片对象
        String pathImage ="C:\\Users\\Administrator.DESKTOP-EKA9NT0\\Desktop\\实训\\PlantImage\\src\\main\\resources\\images\\background.jpg";
        Image image = Toolkit.getDefaultToolkit().getImage(pathImage);

        //把图片对象绘制到窗口里
        g.drawImage(image, 0, 0, null);



    }



}
