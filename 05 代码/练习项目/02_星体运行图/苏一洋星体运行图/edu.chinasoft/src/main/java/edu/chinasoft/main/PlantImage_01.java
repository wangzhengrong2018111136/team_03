package edu.chinasoft.main;

import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class PlantImage_01 extends Frame {

    private int  width=900;
    private int  height=900;
    private Image Screen=null;

    public static void main(String[] args) {
        PlantImage_01 plantImage01 = new PlantImage_01();
        plantImage01.init();

    }

     public void init() {
        this.setSize(width,height);
        this.setLocation(200, 200);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

         ThreadInner threadInner = new ThreadInner();
         threadInner.start();

        this.setVisible(true);
        this.show();
    }

    class   ThreadInner extends Thread{

        // 该线程执行的代码，在run方法
        @Override
        public void run() {
            while(true){
                //repaint方法，实际底层调用的是paint
                PlantImage_01.this.repaint();

            }



        }
    }

    public void update(Graphics ghs) {	//控制屏幕不会闪动
        if(Screen == null)
        {
            Screen = this.createImage(width, height);
        }
        Graphics g = Screen.getGraphics();
        Color oldColor = g.getColor();
        g.setColor(Color.black);
        g.fillRect(0, 0, width, height);
        g.setColor(oldColor);
        paint(g);
        ghs.drawImage(Screen, 0, 0, null);
    }

    @Override
    public  void paint(Graphics g)
    {
        Color colorSrc = g.getColor();

        g.setColor(Color.RED);
        g.drawLine(0,0,500,500);

        g.setColor(Color.BLUE);
        g.drawRect(50,50,250,250);

        g.setColor(Color.yellow);
        g.drawOval(50,50,250,250);
    }

}
