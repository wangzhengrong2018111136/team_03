package wzr.project;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//  当前类是一个窗口
public class PlantImage extends Frame {
    // 表示窗口的宽度和高度
    private int width = 900;
    private int height = 900;
    private Image Screen = null;


    double x , x1 = 00,x2 = 200,x3 = 200,x4 = 200,x5 = 200,x6 = 200,x7 = 200,x8 = 200;
    double y , y1 = 00,y2 = 200,y3 = 200,y4 = 200,y5 = 200,y6 = 200,y7 = 200,y8 = 200;
    double t = 0,t1 = 0,t2 = 0,t3 = 0,t4 = 0,t5 = 0,t6 = 0,t7 = 0,t8 = 0;


    Image sun = Toolkit.getDefaultToolkit().getImage("D:\\Training\\idea\\ideacode\\code2\\src\\main\\resources\\images\\sun.jpg");
    Image background = Toolkit.getDefaultToolkit().getImage("D:\\Training\\idea\\ideacode\\code2\\src\\main\\resources\\images\\background.jpg");
    Image mercury = Toolkit.getDefaultToolkit().getImage("D:\\Training\\idea\\ideacode\\code2\\src\\main\\resources\\images\\Mercury.jpg");//水星
    Image venus = Toolkit.getDefaultToolkit().getImage("D:\\Training\\idea\\ideacode\\code2\\src\\main\\resources\\images\\Venus.jpg");//金星
    Image earth = Toolkit.getDefaultToolkit().getImage("D:\\Training\\idea\\ideacode\\code2\\src\\main\\resources\\images\\Earth.jpg");
    Image mars = Toolkit.getDefaultToolkit().getImage("D:\\Training\\idea\\ideacode\\code2\\src\\main\\resources\\images\\Mars.jpg");//火星
    Image jupiter = Toolkit.getDefaultToolkit().getImage("D:\\Training\\idea\\ideacode\\code2\\src\\main\\resources\\images\\Jupiter.jpg");//木星
    Image saturn = Toolkit.getDefaultToolkit().getImage("D:\\Training\\idea\\ideacode\\code2\\src\\main\\resources\\Saturn.jpg");//土星
    Image uranus = Toolkit.getDefaultToolkit().getImage("D:\\Training\\idea\\ideacode\\code2\\src\\main\\resources\\images\\Uranus.jpg");//天王星
    Image neptune = Toolkit.getDefaultToolkit().getImage("D:\\Training\\idea\\ideacode\\code2\\src\\main\\resources\\images\\Neptune.jpg");//海王星
    Image moon = Toolkit.getDefaultToolkit().getImage("D:\\Training\\idea\\ideacode\\code2\\src\\main\\resources\\images\\moon.jpg");//月亮
    //  整个项目的入口方法
    public static void main(String[] args) {
        PlantImage plantImage = new PlantImage();
        plantImage.init();


    }

    // 显示窗口
    public void init() {
        // 设置窗口的大小
        this.setSize(width, height);
        this.setLocation(200, 200);
        //关闭窗口
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        //  开启一个线程
        ThreadInner threadInner = new ThreadInner();
        threadInner.start();
        // 显示窗口
        this.setVisible(true);
    }

    //  设计一个线程内部类，该类实现调用paint方法
    class ThreadInner extends Thread {

        // 该线程执行的代码，在run方法
        @Override
        public void run() {
            while (true) {
                //repaint方法，实际底层调用的是paint

                PlantImage.this.repaint();
                try {
                    Thread.sleep(24);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


        }
    }

    // 如果通过多线程解决打开窗口不显示问题， 闪动
    public void update(Graphics ghs) {    //控制屏幕不会闪动
        if (Screen == null) {
            Screen = this.createImage(width, height);
        }
        Graphics g = Screen.getGraphics();
        Color oldColor = g.getColor();
        g.setColor(Color.black);
        g.fillRect(0, 0, width, height);
        g.setColor(oldColor);
        paint(g);
        ghs.drawImage(Screen, 0, 0, null);
    }


    //  Graphics  画笔
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.white);//设置轨道颜色
        //设置运行轨道
        g.drawImage(background, 0 , 0 , null);//设置背景星图
        g.drawImage(sun , 435 , 435 , null);
        g.drawOval(401, 415, 88, 60);
        g.drawOval(375, 395, 140, 100);
        g.drawOval(335, 365, 220, 160);
        g.drawOval(285, 335, 320, 220);
        g.drawOval(155, 275, 580, 340);
        g.drawOval(120, 245, 650, 400);
        g.drawOval(80, 210, 730, 470);
        g.drawOval(10, 165, 870, 560);

        //各大行星距太阳的距离依次是：水星、金星、地球、火星、木星、土星、天王星、海王星
        //根据各行星的公转周期，设置其运行速度
        t1 = t1 + Math.PI / 87.9693;
        t2 = t2 + Math.PI / 224.71;
        t3 = t3 + Math.PI / 365;
        t = t + Math.PI / 27;		//设置月亮运动，按月亮自身的公转周期
        t4 = t4 + Math.PI / 686.98;
        t5 = t5 + Math.PI / 4263.2;
        t6 = t6 + Math.PI / 10759.5;
        t7 = t7 + Math.PI / 30799.095;
        t8 = t8 + Math.PI / 60152;
//设置各星的运行
        g.drawImage(mercury , (int)x1 ,(int)y1, null);
        x1 = 440 + 44 * Math.cos(t1);
        y1 = 440 + 30 * Math.sin(t1);
        g.drawImage(venus , (int)x2 ,(int)y2, null);
        x2 = 440 + 70 * Math.cos(t2);
        y2 = 440 + 50 * Math.sin(t2);
        g.drawImage(earth , (int)x3 ,(int)y3, null);
        x3 = 440 + 110 * Math.cos(t3);
        y3 = 440 + 80 * Math.sin(t3);
        g.drawImage(moon , (int)x ,(int)y, null);
        x = x3 + 15 * Math.cos(t);
        y = y3 + 14 * Math.sin(t);
        g.drawImage(mars , (int)x4 ,(int)y4, null);
        x4 = 443 + 160 * Math.cos(t4);
        y4 = 443 + 110 * Math.sin(t4);
        g.drawImage(jupiter , (int)x5 ,(int)y5, null);
        x5 = 437 + 290 * Math.cos(t5);
        y5 = 437 + 170 * Math.sin(t5);
        g.drawImage(saturn , (int)x6 ,(int)y6, null);
        x6 = 438 + 325 * Math.cos(t6);
        y6 = 438 + 200 * Math.sin(t6);
        g.drawImage(uranus , (int)x7 ,(int)y7, null);
        x7 = 439 + 365 * Math.cos(t7);
        y7 = 439 + 235 * Math.sin(t7);
        g.drawImage(neptune , (int)x8 ,(int)y8, null);
        x8 = 439 + 435 * Math.cos(t8);
        y8 = 439 + 280 * Math.sin(t8);
    }
}
